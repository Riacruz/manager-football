﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ManagerFB
{
    class Partido
    {
        public Club local, visitante;
        public string resultado = "";
        public Partido(string partido, bool solo=false)
        {
            var split = partido.Split('-');
            local = DB.GetClub(DB.GetClub(split[0].Trim()));
            visitante = DB.GetClub(DB.GetClub(split[1].Trim()));
            print(local.nombre + " VS " + visitante.nombre);
            print("Estadio "+local.estadio);
            print("Fuerza "+local.mediaConvocados + " VS " + visitante.mediaConvocados);
            //for(int i=0;i<20;i++) 
            Juega(new Control().temporadaActual);
        }
        private void print(string str)
        {
            resultado += str+"\n";
        }
        private void Juega(int temporadaId)
        {
            
            var countLocal = 0;
            var countVisitante = 0;
            for (int i = 0; i < 70; i++)
            {
                var rr = new Random();
                var r = new Random((int)DateTime.UtcNow.ToBinary()+DateTime.Now.Millisecond*i);
                var paLocal = r.Next(0, local.media);//+x de fuerza por local
                r = new Random((int)DateTime.UtcNow.ToBinary() * rr.Next(1, 1000)*i);
                var paVisitante = r.Next(0, visitante.media);
                paLocal = paLocal + local.media;
                paVisitante = paVisitante + visitante.media;
                // r = new Random();
                r = new Random((int)DateTime.UtcNow.ToBinary() + rr.Next(1, 1000));
                float randLocal = r.Next(0, 9) * 0.1f;
                r = new Random((int)DateTime.UtcNow.ToBinary() + rr.Next(1, 1000));
                float randVisitante = r.Next(0, 9) * 0.1f;
                r = new Random((int)DateTime.UtcNow.ToBinary() + rr.Next(1, 1000));
                int variable = r.Next(10, 30);
                r = new Random((int)DateTime.UtcNow.ToBinary() + rr.Next(1, 1000));
                var superAtaque = r.Next(3, 5);
                int fuerzaSuperAtaque = 100;
                if(superAtaque== 3)
                {
                    paLocal = paLocal + fuerzaSuperAtaque;

                } else if (superAtaque == 4)
                {
                    paVisitante = paVisitante + fuerzaSuperAtaque;
                }
                //Util.Speak((int)(paLocal * randLocal) + " " + (int)(paVisitante * randVisitante));

                if ((int)(paLocal * randLocal) > (int)(paVisitante * randVisitante))
                {
                    if ((int)(paLocal * randLocal) - variable <= (int)(paVisitante * randVisitante))
                        continue;
                    
                   // Util.Speak("Gol local");
                    countLocal++;
                }
                else if ((int)(paLocal * randLocal) == (int)(paVisitante * randVisitante))
                {
                    continue;
                }
                else
                {
                    if ((int)(paLocal * randLocal) > (int)(paVisitante * randVisitante) - variable)
                        continue;
                    //Util.Speak("Gol visitante");
                    countVisitante++;
                }
            }
            var rand = new Random((int)DateTime.UtcNow.ToBinary());
            var varLoc = rand.Next(0, 3);
            rand = new Random((int)DateTime.UtcNow.ToBinary());
            var varVis = rand.Next(0, 3);
            //Util.Speak(varLoc + " " + varVis);
            countLocal = (int)(countLocal * (0.1f-(varLoc*0.05f)));
            countVisitante = (int)(countVisitante * (0.1f - (varVis * 0.05f)));
            print("Resultado "+countLocal + " - "  + countVisitante);
            float aforoConseguido = local.aforo * 5 / local.precioEntrada;
            aforoConseguido = aforoConseguido + (rand.Next(0, (local.aforo - (int)aforoConseguido)) / DB.GetPosicionClub(local));
            print("Aforo " + aforoConseguido + " personas " + (((int)aforoConseguido * 100 / local.aforo)) + " por ciento de aforo");
            Util.clubsLocalesUltimaJornada.Add(local);
            Util.aforosUltimaJornada.Add((int)aforoConseguido);
            var minuto = 1;
            List<int> minutosCogidos = new List<int>();
            if(countLocal>0)print("Goles "+local.nombre+":");
            string partido = local.nombre + " - " + visitante.nombre;
            for (int i=0;i<countLocal;i++)
            {
                var r = new Random();
                var gol = new Gol();
                var _minuto = minuto;
                while (minutosCogidos.Contains(_minuto))
                {
                    _minuto = r.Next(minuto, 100);
                  // minuto = _minuto;
                    //  Thread.Sleep(1);
                }
                gol.minuto = _minuto;
                minutosCogidos.Add(_minuto);
                r = new Random((int)DateTime.UtcNow.ToBinary()+minuto);
                Thread.Sleep(r.Next(1, 10));                
               // var jug = local.lJugadores[r.Next(0, (local.lJugadores.Count))];
                var jug = EligeGoleador(local.lJugadores);
                gol.jugador = jug.nombre;
                gol.club = local;
                int visGoles = visitante.golesEnContra + 1;
                int locGoles = local.golesAFavor + 1;
                DB.query($"INSERT INTO Gol (minuto, idJudagor, idClub, partido, temporadaId) VALUES ('{_minuto}','{jug.id}','{local.id}','{partido}','{temporadaId}');");                
                DB.query($"UPDATE Club SET golesAFavor='{locGoles}' WHERE id='{local.id}'");
                DB.query($"UPDATE Club SET golesEnContra='{visGoles}' WHERE id='{visitante.id}'");
                print("Minuto " + _minuto + " " + gol.jugador);
                
            }
            //minuto = 0;
            if(countVisitante>0)print("Goles "+visitante.nombre+":");
            for (int i = 0; i < countVisitante; i++)
            {
                var r = new Random();
                var gol = new Gol();
                var _minuto = minuto;
                while (minutosCogidos.Contains(_minuto))
                {
                    _minuto = r.Next(minuto, 100);
                   // minuto = _minuto;
                   // Thread.Sleep(1);
                }
                gol.minuto = _minuto;
                minutosCogidos.Add(_minuto);
                r = new Random((int)DateTime.UtcNow.ToBinary()+minuto);
                Thread.Sleep(r.Next(1,10));
                //Jugador jug = visitante.lJugadores[r.Next(0, (visitante.lJugadores.Count))];
                var jug = EligeGoleador(visitante.lJugadores);
                gol.jugador = jug.nombre;
                gol.club = visitante;
                int visGoles = visitante.golesAFavor + 1;
                int locGoles = local.golesEnContra + 1;
                DB.query($"INSERT INTO Gol (minuto, idJudagor, idClub, partido, temporadaId) VALUES ('{_minuto}','{jug.id}','{visitante.id}','{partido}','{temporadaId}');");
                DB.query($"UPDATE Club SET golesAFavor='{visGoles}' WHERE id='{visitante.id}'");
                DB.query($"UPDATE Club SET golesEnContra='{locGoles}' WHERE id='{local.id}'");
                print("Minuto " + _minuto + " " + gol.jugador);
            }
            DB.query("INSERT INTO Resultado (local, visitante, localGolesId, visitanteGolesId, temporadaId, jornada) "+
                $"VALUES ('{local.nombre}','{visitante.nombre}','{countLocal}','{countVisitante}','{temporadaId}','{new Control().jornadaActual}');");
        }

        private Jugador EligeGoleador(List<Jugador> lJugadores)
        {
            var r = new Random((int)DateTime.UtcNow.ToBinary());
            List<string> puestos = new List<string>(){ "Portero", "Defensa central","Defensa central", "Defensa central","Defensa central",
                "Lateral izquierdo", "Lateral izquierdo", "Lateral izquierdo", "Lateral izquierdo", 
                "Lateral derecho", "Lateral derecho", "Lateral derecho", "Lateral derecho", 
                "Pivote", "Pivote", "Pivote", "Pivote", "Pivote", "Pivote", "Pivote", 
                "Medio centro", "Medio centro", "Medio centro", "Medio centro", "Medio centro", "Medio centro", 
                "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", "Medio centro ofensivo", 
                "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", "Extremo izquierdo", 
                "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", "Extremo derecho", 
                "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro", "Delantero centro"
            };

            
            var jugTitulares = lJugadores.Select(n => n).Where(n => n.convocado == 1).ToList();
            List<Jugador> jugPuesto = new List<Jugador>();
            while (jugPuesto.Count == 0)
            {
                jugPuesto = jugTitulares.Select(n => n).Where(n => n.puesto.Equals(puestos[r.Next(0, puestos.Count)])).ToList();
            }
            var jug = jugPuesto[r.Next(0, (jugPuesto.Count))];
            return jug;
        }
    }
}

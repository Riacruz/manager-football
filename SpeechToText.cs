﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ManagerFB
{
    public static class SpeechToText
    {
        public static bool escuchando = false;
        public static int intPalabras = 0;
        static Choices phrases = new Choices();
        static SpeechRecognitionEngine recognizer =
              new SpeechRecognitionEngine(
                new System.Globalization.CultureInfo("es"));
        static Process proc = new Process();
        // Handle the SpeechRecognized event.  
        static void recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result.Text.ToLower().Contains("comando") && !escuchando)
            {
                escuchando = true;
                Util.Speak("comando");
                return;
            }
            if (escuchando)
            {
                escuchando = false;
                var result = FraseLetrasANumeros(e.Result.Text);
                Program.line = result.Replace("comando", "");
                Program.key = ConsoleKey.Enter;
                SendKeys.SendWait("{ENTER}");
                //Program.leeLinea(e.Result.Text.Replace("Comando", ""));
                // Console.WriteLine(e.Result.Text.Replace("OK manager", ""));                
                return;
            }
        }
        private static string FraseLetrasANumeros(string frase)
        {
            string str = frase;
            string[] lNumeros = { "cero","uno","dos","tres","cuatro","cinco","seis","siete","ocho","nueve","diez","once","doce","trece","catorce","quince","dieciseis","diecisiete","dieciocho","diecinueve" };
            int[] lNumerosInt = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19 };
            for (int i=0;i<lNumeros.Length;i++)
            {
                if (str.Contains(" " + lNumeros[i]))
                {
                    str = str.Replace(" " + lNumeros[i], " "+lNumerosInt[i]);
                }

            }
            return str;
        }
        public static void Speech2()
        {
            // Create and load a dictation grammar.
            //Phrases that will be recognised added
            
            List<string> lComandos = new List<string>();
            foreach (var j in Globals.nombresClubs)
            {
                lComandos.Add("jugadores " + j);
                lComandos.Add("convocados " + j);
                lComandos.Add("titulares " + j);
                lComandos.Add("suplentes " + j);
                lComandos.Add("club " + j);
                lComandos.Add("jugadores " + j + " media");
                for (int i = 0; i < 100; i++)
                {
                    lComandos.Add("jugador " + i + " " + j);
                }
            }
            for (int i = 0; i < 500; i++)
            {
                lComandos.Add("entrada " + i);
            }
            for (int i = 0; i < 100; i++)
            {
                lComandos.Add("jugadores edad " + i);
                lComandos.Add("plantilla " + i);
                lComandos.Add("resultados " + i);
                lComandos.Add("vender " + i);
                lComandos.Add("jornada " + i);
                lComandos.Add("titular " + i);
                lComandos.Add("suplente " + i);
                lComandos.Add("desconvocar " + i);
            }
            for (int i = 0; i < 100; i++)
            {
                for (int x = 0; x < 100; x++)
                {
                    lComandos.Add("cambio " + i + " " + x);
                }
            }
            string [] comandos = {
                   "Comando",
                   "continuar",
                   "nueva partida",
                   "plantilla",
                   "club",
                   "clubs",
                   "tabla",
                   "clasificacion",
                   "resultados",
                   "jornada",
                   "jugadores",
                   "convocados",
                   "titulares",
                   "suplentes",
                   "posicion",
                   "traspasos",
                   "traspasos media",
                   "ofertas",
                   "pichichi",
                   "patrocinador",
                   "jugar",
                   "jugar solo",
                   "j",
                   "musica 0",
                   "musica 1",
                   "volumen 0",
                   "volumen 1"
            };
            lComandos.AddRange(comandos);
            string[] com = lComandos.ToArray();
            // lComandos.AddRange(comandos.ToList());
            Choices cho = new Choices(com);
            Choices phrases = new Choices();
            phrases.Add("Comando",
                "continuar",
                "nueva partida",
                "club",
                "clubs",
                "tabla",
                "clasificacion",
                "resultados",
                "jornada",
                "jugadores",
                "convocados",
                "titulares",
                "suplentes",
                "posicion",
                "traspasos",
                "traspasos media",
                "ofertas",
                "pichichi",
                "patrocinador",
                "jugar",
                "jugar solo",
                "j",
                "musica 0",
                "musica 1",
                "volumen 0",
                "volumen 1",
                "cerruda",
                "Estrella CF"
                );
            GrammarBuilder gb = new GrammarBuilder("comando",0,2);
           // GrammarBuilder gb = new GrammarBuilder();
            //adding our phrases to the grammar builder
            gb.Append(phrases);
           // gb.Append(lComandos);
            recognizer.LoadGrammar(new Grammar(gb));
            recognizer.LoadGrammar(new DictationGrammar("grammar:dictation"));
            // Add a handler for the speech recognized event.  
            recognizer.SpeechRecognized +=
                new EventHandler<SpeechRecognizedEventArgs>(recognizer_SpeechRecognized);

            // Configure input to the speech recognizer.  
            recognizer.SetInputToDefaultAudioDevice();

            // Start asynchronous, continuous speech recognition.  
            recognizer.RecognizeAsync(RecognizeMode.Multiple);
            // Console.WriteLine("oad");
            // Keep the console window open.      
        }
        public static void Speech()
        {
            // Create an in-process speech recognizer for the en-US locale.  
            using (
            SpeechRecognitionEngine recognizer =
              new SpeechRecognitionEngine(
                new System.Globalization.CultureInfo("es")))
            {
                // Create and load a dictation grammar.
                //Phrases that will be recognised added
                Choices phrases = new Choices();
                phrases.Add("Comando",
                    "continuar",
                    "nueva partida",
                    "club",
                    "clubs",
                    "tabla",
                    "clasificacion",
                    "resultados",
                    "jornada",
                    "jugadores",
                    "convocados",
                    "titulares",
                    "suplentes",
                    "posicion",
                    "traspasos",
                    "traspasos media",
                    "ofertas",
                    "pichichi",
                    "patrocinador",
                    "jugar",
                    "jugar solo",
                    "j",
                    "musica 0",
                    "musica 1",
                    "volumen 0",
                    "volumen 1",
                    "jugador 11 cerruda",
                    "Manolo"
                    );
                GrammarBuilder gb = new GrammarBuilder();
                //adding our phrases to the grammar builder
                gb.Append(phrases);
                recognizer.LoadGrammar(new Grammar(gb));

                // Add a handler for the speech recognized event.  
                recognizer.SpeechRecognized +=
                  new EventHandler<SpeechRecognizedEventArgs>(recognizer_SpeechRecognized);

                // Configure input to the speech recognizer.  
                recognizer.SetInputToDefaultAudioDevice();

                // Start asynchronous, continuous speech recognition.  
                recognizer.RecognizeAsync(RecognizeMode.Multiple);
               // Console.WriteLine("oad");
                // Keep the console window open.  
                while (true)
                {
                    //Console.ReadLine();
                }
               
            }

        }

    }
}

# Mánager de fútbol para mi hermano

Juego de tipo mánager de fútbol en consola pensado para que puedan jugar personas invidentes.

- Juega una liga con 8 equipos
- Lector de textos
- Ayuda en cada sección
- La primera vez que empiezas una Nueva Temporada crea aleatoriamente todos los jugadores de todos los clubs y sus características.
- Traspaso de jugadores
- Ofertas en pretemporada
- Cambio de jugadores y alineación
- Base de datos persiste entre temporadas
- Guardado automático

![](https://islandfightersshop.com/ppp/managerfb.png)
* En la versión de producción no se ve ningún texto, solo se escucha
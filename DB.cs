﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ManagerFB
{
    public static class DB
    {
        public static List<List<string>> query(string consulta)
        {
            List<List<string>> list = new List<List<string>>();
            try
            {

                // string dbFile = Path.Combine(Path.GetDirectoryName(Environment.CurrentDirectory), "ManagerFB.mdf");
                //  string strBuilder = $"{Path.GetDirectoryName(Environment.CurrentDirectory)};Initial Catalog=ManagerFB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                // string strBuilder = @"Data Source=""" + Path.GetDirectoryName(Environment.CurrentDirectory) + @""";Initial Catalog=ManagerFB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                // string strBuilder = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ManagerFB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                //  string strBuilder = @"Data Source=(localdb)" + Path.GetDirectoryName(Environment.CurrentDirectory) + @";Initial Catalog=ManagerFB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                // string strBuilder = @"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename="""+Path.GetDirectoryName(Environment.CurrentDirectory)+@"""\ManagerFB.mdf;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                //Util.Speak(strBuilder);
                //  SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(strBuilder);
                string conectionString = @"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=" + Directory.GetCurrentDirectory() + @"\ManagerFB.mdf;Integrated Security=True";
#if DEBUG
              //  conectionString = @"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=" + Path.GetDirectoryName(Directory.GetCurrentDirectory()) + @"\ManagerFB.mdf;Integrated Security=True";
#else
                conectionString = @"Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=" + Path.GetDirectoryName(Directory.GetCurrentDirectory()) + @"\ManagerFB.mdf;Integrated Security=True";
#endif
                // using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                using (SqlConnection connection = new SqlConnection(conectionString))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(consulta);
                  //  Util.Speak(consulta);
                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                List<string> li = new List<string>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    li.Add(reader.GetSqlValue(i).ToString());
                                }
                                list.Add(li);
                            }
                        }
                    }
                }

            }
            catch (SqlException exc)
            {
                Util.Speak(exc.ToString());
            }
            return list;
        }        
        public static void DeleteAll()
        {
            DB.query("DELETE FROM Calendario;");
            DB.query("DELETE FROM Jugador;");
            DB.query("DELETE FROM Club;");
            DB.query("DELETE FROM Gol;");
            DB.query("DELETE FROM Resultado;");
            DB.query("DELETE FROM Temporada;");
            DB.query("DELETE FROM Control;");
            DB.query("DELETE FROM Oferta;");
        }
        public static void DeleteControl()
        {
            DB.query("DELETE FROM Control;");
        }
        /// <summary>
        /// Devuelve la id del club por el nombre
        /// </summary>
        /// <param name="nombre">nombre del club</param>
        /// <returns></returns>
        public static int GetClub(string nombre)
        {
            var club = new Club();
            var q = DB.query($"SELECT id FROM Club WHERE nombre='{nombre.Trim()}';");
            var c = q[0];
            return int.Parse(c[0]);
        }
        /// <summary>
        /// Devuelve List<Jugador> con jugadores de tal club
        /// </summary>
        /// <param name="nombreClub">nombre del club</param>
        /// <returns></returns>
        public static List<Jugador> GetJugadoresClub(string nombreClub)
        {
            var lJug = new List<Jugador>();
            var q = DB.query($"SELECT * FROM Jugador WHERE club='{nombreClub.Trim()}';");
            lJug.Sort();
            foreach (var l in q)
            {
                Jugador j = new Jugador();
                j.nombre = l[0];
                j.club = l[1];
                j.edad = int.Parse(l[2]);
                j.fuerza = int.Parse(l[3]);
                j.velocidad = int.Parse(l[4]);
                j.resistencia = int.Parse(l[5]);
                j.id = int.Parse(l[6]);
                j.dorsal = int.Parse(l[7]);
                j.puesto = l[8];
                j.convocado = int.Parse(l[9]);
                j.titular = int.Parse(l[10]);
                j.sueldo = int.Parse(l[11]);
                j.clausula = int.Parse(l[12]);
                j.enTraspaso = int.Parse(l[13]);
                j.contratoRestante = int.Parse(l[14]);
                lJug.Add(j);
            }
            return lJug;
        }
        public static Jugador GetJugadorId(int id)
        {
            var lJug = new List<Jugador>();
            var q = DB.query($"SELECT * FROM Jugador WHERE id='{id}';");
            var l = q[0];
            Jugador j = new Jugador();
            j.nombre = l[0];
            j.club = l[1];
            j.edad = int.Parse(l[2]);
            j.fuerza = int.Parse(l[3]);
            j.velocidad = int.Parse(l[4]);
            j.resistencia = int.Parse(l[5]);
            j.id = int.Parse(l[6]);
            j.dorsal = int.Parse(l[7]);
            j.puesto = l[8];
            j.convocado = int.Parse(l[9]);
            j.titular = int.Parse(l[10]);
            j.sueldo = int.Parse(l[11]);
            j.clausula = int.Parse(l[12]);
            j.enTraspaso = int.Parse(l[13]);
            j.contratoRestante = int.Parse(l[14]);
            lJug.Add(j);            
            return j;
        }
        public static Jugador GetJugadorDorsal(int dorsal)
        {
            var lJug = new List<Jugador>();
            var q = DB.query($"SELECT * FROM Jugador WHERE dorsal='{dorsal}' AND club='{new Control().clubActual}';");
            var l = q[0];
            Jugador j = new Jugador();
            j.nombre = l[0];
            j.club = l[1];
            j.edad = int.Parse(l[2]);
            j.fuerza = int.Parse(l[3]);
            j.velocidad = int.Parse(l[4]);
            j.resistencia = int.Parse(l[5]);
            j.id = int.Parse(l[6]);
            j.dorsal = int.Parse(l[7]);
            j.puesto = l[8];
            j.convocado = int.Parse(l[9]);
            j.titular = int.Parse(l[10]);
            j.sueldo = int.Parse(l[11]);
            j.clausula = int.Parse(l[12]);
            j.enTraspaso = int.Parse(l[13]);
            j.contratoRestante = int.Parse(l[14]);
            lJug.Add(j);            
            return j;
        } 
        /// <summary>
        /// Devuelve un Club por el id
        /// </summary>
        /// <param name="id">id del club</param>
        /// <returns></returns>
        public static Club GetClub(int id)
        {
            var club = new Club();
            var q = DB.query($"SELECT * FROM Club WHERE id='{id}';");
            var c = q[0];
            club.nombre = c[0];
            club.estadio = c[1];
            club.presupuesto = int.Parse(c[2]);
          //  club.golesAFavor = int.Parse(c[4]);
          //  club.golesEnContra = int.Parse(c[5]);
            club.id = int.Parse(c[3]);
            if (!c[8].Equals("Null")) club.aforo = int.Parse(c[8]);
            if (!c[9].Equals("Null")) club.precioEntrada = int.Parse(c[9]);
            club.lJugadores = new List<Jugador>();
            club.objetivo = c[10];
            club.patrocinador = c[6];
            q = DB.query($"SELECT * FROM Jugador WHERE club='{club.nombre}'");            
            foreach (var l in q)
            {
                var jug = new Jugador();
                jug.nombre = l[0];
                jug.club = l[1];
                jug.edad = int.Parse(l[2]);
                jug.fuerza = int.Parse(l[3]);
                jug.velocidad = int.Parse(l[4]);
                jug.resistencia = int.Parse(l[5]); ;
                jug.id = int.Parse(l[6]);
                if(!l[7].Equals("Null"))jug.dorsal = int.Parse(l[7]);
                if (!l[8].Equals("Null")) jug.puesto = l[8];
                if (!l[9].Equals("Null")) jug.convocado = int.Parse(l[9]);
                if (!l[10].Equals("Null")) jug.titular = int.Parse(l[10]);
                if (!l[11].Equals("Null")) jug.sueldo = int.Parse(l[11]);
                if (!l[12].Equals("Null")) jug.clausula = int.Parse(l[12]);
                if (!l[12].Equals("Null")) jug.enTraspaso = int.Parse(l[13]);
                if (!l[12].Equals("Null")) jug.contratoRestante = int.Parse(l[14]);
                club.lJugadores.Add(jug);
            }
            return club;
        } 
        /// <summary>
        /// Devuelve un string con los id de los clubs tipo 1,3,2,12,5... (Para almacenar en temporada DB)
        /// </summary>
        public static string GetIdClubs()
        {
            var q = DB.query("SELECT id FROM Club;");
            var str = "";
            foreach (var l in q)
            {
                str += l[0] + ",";
            }
            return str;
        } 
        /// <summary>
        /// Devuelve un List<string> son los clubs en la DB
        /// </summary>
        public static List<string> GetNombreClubs()
        {
            var q = DB.query("SELECT nombre FROM Club;");
            var lFin = new List<string>();
            foreach (var l in q)
            {
                lFin.Add(l[0]);
            }
            return lFin;
        }
        public static int GetPosicionClub(Club club)
        {
            var q = DB.query($"SELECT * FROM Resultado WHERE temporadaId='{new Control().temporadaActual}'");
            var str = "";
            Club[] clubs = new Temporada().lClubs.ToArray();
            int[] puntosClubs = new int[clubs.Length];
            //str += "Temporada " + new Temporada().nombre + " Jornada " + new Control().jornadaActual+"\n";
            foreach (var r in q)
            { //local, visitante, localGoles, visitanteGoles, temporadaId
                for (int i = 0; i < clubs.Length; i++)
                {
                    int localGoles = int.Parse(r[2]);
                    int visitanteGoles = int.Parse(r[3]);
                    if (r[0].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles > visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 3;
                    }
                    else if (r[1].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles < visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 3;
                    }
                    else if (r[0].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 1;
                    }
                    else if (r[1].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 1;
                    }
                }
            }
            Array.Sort(puntosClubs, clubs);
            Array.Reverse(clubs);
            Array.Reverse(puntosClubs);
            int indexFin = 0;
            for (int i = 0; i < clubs.Length; i++)
            {
                if (clubs[i].nombre.Equals(club.nombre)) return (i + 1);
                str += (i + 1) + "º " + clubs[i].nombre + ", " + puntosClubs[i] + " puntos, " + clubs[i].golesAFavor + " goles a favor, " + clubs[i].golesEnContra + " goles en contra" + "\n";
            }
            return indexFin;
        }
        /// <summary>
        /// Añade temporada
        /// </summary>
        /// <param name="nombre">El nombre de la temporada</param>
        public static void AddTemporada(string nombre, int temporada=1)
        {
            var cal = new Calendario();
            cal.CreaNuevoCalendario(GetNombreClubs());
            var clubsId = GetIdClubs();
            DB.query("INSERT INTO Temporada (nombre,jornadaActual,calendarioId,clubsId)" +
                    $"VALUES ('{nombre}','{1}','{AddCalendario(cal.LeeCalendario(),temporada)}','{clubsId}');");
            
        } 
        /// <summary>
        /// Añade calendario
        /// </summary>
        /// <param name="calendario">Lista calendario</param>
        public static int AddCalendario(List<string> calendario, int temporadaId)
        {
            for (int i = 0; i < calendario.Count; i++)
            {
                string partido = calendario[i];
                int posicion = i;
                DB.query("INSERT INTO Calendario (partido,posicion,temporadaId)" +
                        $"VALUES ('{partido}','{posicion}','{temporadaId}');");
               // var res = DB.query($"SELECT temporadaId FROM Calendario WHERE partido='{partido}' AND temporadaId='{temporadaId}';");
               // id = int.Parse(res[0][0]);
            }
            return temporadaId;
        } 
        /// <summary>
        /// Añade lista de clubs con una lista de nombres y nombres de estadios
        /// </summary>
        /// <param name="clubs">Lista de clubs a añadir</param>
        /// <param name="estadios">Lista de estadios</param>
        /// <param name="crearJugadoresRandom">True crea 20 jugadores aleatorios para cada club</param>
        public static void AddClubs(List<string> clubs, List<string> estadios, List<Patrocinador> lPats, bool crearJugadoresRandom=false)
        {
            for (int i = 0; i < clubs.Count; i++)
            {
                string patrocinador = "Sin patrocinador";
                /*
                if (lPats.Count > 0)
                {
                    patrocinador = lPats[0].nombre;
                    lPats.RemoveAt(0);
                }
                */
                string nombre = clubs[i];
                string estadio = estadios[i];
                var r = new Random();
                int presupuesto = r.Next(100000, 500000);
                int aforo = r.Next(500, (int)presupuesto/100);
                string[] objetivos = { "Primeros puestos", "Media tabla" };
                var objetivo = objetivos[r.Next(0, objetivos.Length)];
                DB.query("INSERT INTO Club (nombre,estadio,presupuesto, golesAFavor,golesEnContra,patrocinador,temporadasJugadas,aforo,precioEntrada, objetivo)" +
                        $"VALUES ('{Util.UppercaseFirst(nombre)}','{estadio}','{presupuesto}','{0}','{0}','{patrocinador}','0','{aforo}','10','{objetivo}');");
                if (crearJugadoresRandom) AddJugadoresRandom(20, nombre);
            }
        }
        /// <summary>
        /// Añade aleatoriamente tal cantidad de jugadores a tal club
        /// </summary>
        /// <param name="cantidad">La cantidad de jugadores a añadir</param>
        /// <param name="club">El nombre del club</param>
        public static void AddJugadoresRandom(int cantidad, string club, int maxEdad=50)
        {
            List<string> puestos = new List<string>(){ "Portero", "Defensa central","Defensa central", "Lateral izquierdo", "Lateral derecho", "Pivote", "Medio centro", "Medio centro ofensivo",
                "Extremo izquierdo","Extremo derecho","Delantero centro", "Portero", "Defensa central","Defensa central", "Lateral izquierdo", "Lateral derecho", "Pivote", "" +
                "Medio centro", "Medio centro ofensivo", "Extremo izquierdo","Extremo derecho","Delantero centro"};
            int _convocado = 17;
            int _titular = 11;
            List<int> dorsalesCogidos = new List<int>();
            for (int i = 0; i < cantidad; i++)
            {
                // string[] lVars = { "mi", "ma"};
                string[] lFinales = { "ulio", "ini", "elba", "olo", "quin","son","amo","pe","ez","ar","unez","ios","uz","iles","les","ana","orla",
                    "irez","ero","tiz","erez","ante","ason","ovik","oslaf","baf","ix","ius","us","ula"};
                string consonante = "wrtyplkjhgfdszxcvbnm";
                string vocal = "aeiouy";
                
                var r = new Random((int)DateTime.Now.ToBinary()*i);
                if (i == 0) r = new Random((int)DateTime.Now.ToBinary());
                int silabas = r.Next(0, 2);
                string nombre = "" + consonante[r.Next(0, consonante.Length)] + vocal[r.Next(0, vocal.Length)];
                for (int s = 0; s < silabas; s++)
                {
                    nombre += "" + consonante[r.Next(0, consonante.Length)] + vocal[r.Next(0, vocal.Length)];
                }
                nombre += lFinales[r.Next(0, lFinales.Length)];
                //  Util.Speak(nombre);
                int edad = r.Next(18, maxEdad);
                int fuerza = r.Next(30, 100);
                int velocidad = r.Next(30, 100);
                int resistencia = r.Next(30, 100);
                int dorsal = 1;
                string puesto = puestos[0];
                puestos.RemoveAt(0);
                while (dorsalesCogidos.Contains(dorsal))
                {
                    dorsal = r.Next(1, cantidad+1);
                }
                dorsalesCogidos.Add(dorsal);
                int titular = 0;
                int convocado = 0;
                if(_titular>0)
                {
                    _titular--;
                    _convocado--;
                    titular = 1;
                    convocado = 1;
                } else if(_convocado>0)
                {
                    _convocado--;
                    convocado = 1;
                }
                var minimoSueldo = ((fuerza + velocidad + resistencia) / 3) + 20;
                int sueldo = r.Next(minimoSueldo*2, minimoSueldo*4);
                int clausula = sueldo * r.Next(20, 40)*5;
                int[] prob = { 0,0,0,0,0,0,0,0,1 };
                int enTraspaso = prob[r.Next(0, prob.Length)];
                if (club.Equals(new Control().clubActual)) enTraspaso = 0;
                if (club.Equals("Sin club")) enTraspaso = 1;
                int contratoRestante = r.Next(1, 5);
               // Console.WriteLine(enTraspaso);
                DB.query("INSERT INTO Jugador (nombre,club,edad,fuerza,velocidad,resistencia,dorsal, puesto, titular, convocado, sueldo, clausula,enTraspaso, contratoRestante)" +
                        $"VALUES ('{Util.UppercaseFirst(nombre)}','{club}','{edad}','{fuerza}','{velocidad}','{resistencia}','{dorsal}','{puesto}','{titular}'," +
                        $"'{convocado}','{sueldo}','{clausula}','{enTraspaso}','{contratoRestante}');");
            }
        }  
        public static void RemoveRandomJugadoresEnTraspaso()
        {
            var r = new Random();
            int[] prob = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 };
            var q = DB.query("SELECT * FROM Jugador WHERE enTraspaso='1'");
            foreach (var j in q)
            {
                if(j[1].Equals("Sin club"))
                {
                    DB.query($"UPDATE Jugador SET enTraspaso='1' WHERE id='{j[6]}'");
                }
                else
                {
                    int enTraspaso = prob[r.Next(0, prob.Length)];
                    if (!j[1].Equals(new Control().clubActual))
                        DB.query($"UPDATE Jugador SET enTraspaso='{enTraspaso}' WHERE id='{j[6]}'");
                }
            }
        }    
        public static void AddRandomJugadoresEnTraspaso()
        {
            var r = new Random();
            int[] prob = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 };
            var q = DB.query("SELECT * FROM Jugador WHERE enTraspaso='0'");
            foreach (var j in q)
            {
                if(j[1].Equals("Sin club"))
                {
                    DB.query($"UPDATE Jugador SET enTraspaso='1' WHERE id='{j[6]}'");
                }
                else
                {
                    int enTraspaso = prob[r.Next(0, prob.Length)];
                    if (!j[1].Equals(new Control().clubActual))
                        DB.query($"UPDATE Jugador SET enTraspaso='{enTraspaso}' WHERE id='{j[6]}'");
                }
            }
        }  
        public static void CambiaJugadoresEnTraspaso()
        {
            var r = new Random();
            int[] prob = { 0, 0, 0, 0, 0, 0, 0, 0, 1 };
            var q = DB.query("SELECT * FROM Jugador");
            foreach (var j in q)
            {
                if(j[1].Equals("Sin club"))
                {
                    DB.query($"UPDATE Jugador SET enTraspaso='1' WHERE id='{j[6]}'");
                }
                else
                {
                    int enTraspaso = prob[r.Next(0, prob.Length)];
                    if (!j[1].Equals(new Control().clubActual))
                        DB.query($"UPDATE Jugador SET enTraspaso='{enTraspaso}' WHERE id='{j[6]}'");
                }
            }
        }
        public static void Reset(string nombreClub)
        {
            DeleteAll();
            query($"INSERT INTO Control (jornadaActual, temporadaActual, clubActual, primeraPartida) VALUES ('-5', '1', '{nombreClub}','1');");
            AddClubs(Globals.nombresClubs.ToList(), Globals.estadios.ToList(), Globals.lPatrocinadores, true);
            AddTemporada("2020");
            Lector.isPretemporada = true;
        }
        public static string NuevaPartida()
        {
            Reset("");
            var lClubs = new Temporada().lClubs;
            int indexClub = 0;
            if (!Util.SiNo("¿Estás seguro? La partida anterior se borrará"))
            {
                return "Nueva partida cancelada";
            }
            List<string> list = lClubs.Select(n => n.nombre + ", Objetivo " + n.objetivo).ToList();
            Util.Speak("Elige un equipo, " + list[0]);
            indexClub = Util.SelectFromList(list);
            var str = "Empieza nueva partida con el equipo " + lClubs[indexClub].nombre;
            query($"UPDATE Control SET clubActual='{lClubs[indexClub].nombre}'");
            str += ", Temporada " + new Temporada().nombre + " Jornada " + (new Control().jornadaActual + 6) + " de la pretemporada\n";
            Util.EstadoDeJuego = Util.state.EnPartida;
            return str;
        }

        public static void NuevaTemporada(string nombre, int temporada, string clubActual)
        {
            DeleteControl();
            AddTemporada(nombre, temporada);
            query($"INSERT INTO Control (jornadaActual, temporadaActual, clubActual, primeraPartida) VALUES ('-5', '{temporada}', '{clubActual}', '1');");
            CambiaJugadoresEnTraspaso();
            Lector.isPretemporada = true;
        }


    }
}


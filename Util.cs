﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;

namespace ManagerFB
{
    public static class Util
    {
        public static bool musicON = true;
        public static List<int> aforosUltimaJornada = new List<int>();
        public static List<Club> clubsLocalesUltimaJornada = new List<Club>();
        // Initialize a new instance of the SpeechSynthesizer.  
        private static SpeechSynthesizer synth = new SpeechSynthesizer();
        static IWavePlayer waveOutDevice = new WaveOut();
        static IWavePlayer waveOutDeviceAmbient = new WaveOut();
        static AudioFileReader audioFileReaderMusic;
        static AudioFileReader audioFileReaderAmbient;
        public enum state {
            Inicio, EnPartida
        };
        public static state EstadoDeJuego = state.Inicio;
        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
        public static string SecureStringToLower(string str)
        {
            var s = str.Replace("á", "a");
            s = s.Replace("é", "e");
            s = s.Replace("í", "i");
            s = s.Replace("ó", "o");
            s = s.Replace("ú", "u");
            s = s.ToLower();
            return s;
        }
        public static void SpeakSpeed(int speed)
        {
            if (speed < 1) speed = 1;
            if (speed > 4) speed = 4;
            synth.Rate = speed;
            DB.query($"UPDATE Config SET velocidadLector='{speed}'");
        }
        public static void Speak(string str)
        {
            if (String.IsNullOrWhiteSpace(str)) return;
            synth.SpeakAsyncCancelAll();            
            synth.Volume = 100;
            // Configure the audio output.  
            if (synth.State == SynthesizerState.Ready)
                synth.SetOutputToDefaultAudioDevice();
            // Speak a string.  
            if(!string.IsNullOrWhiteSpace(str))synth.SpeakAsync(str);
            synth.Resume();
            if (str.Length < 2) return;
            Console.WriteLine(str);
            
        }
        public static void PlayMusic(int i, bool isInicio=true)
        {
            if (i==1 && isInicio)
            {
                audioFileReaderMusic = new AudioFileReader(Directory.GetCurrentDirectory() + @"\insideTheBox.mp3");
                waveOutDevice.Volume = 1;
                waveOutDevice.Init(audioFileReaderMusic);
                audioFileReaderMusic.Volume = 0.15f;
                audioFileReaderMusic.Position = 999999*2;
                waveOutDevice.Play();
                waveOutDevice.PlaybackStopped += (sender,e) => 
                {
                    if (musicON)
                    {
                        audioFileReaderMusic.Position = 999999 * 2;
                        waveOutDevice.Play();
                    }
                };
            } 
            else if(i==0)
            {
                DB.query("UPDATE Config SET musica='0'");
                musicON = false;
                waveOutDevice.Stop();
            }
            else if(i==1)
            {
                DB.query("UPDATE Config SET musica='1'");
                musicON = true;
                waveOutDevice.Play();
            }
        }  
        public static void PlayAmbient(int i)
        {
            if (i==1)
            {
                audioFileReaderAmbient = new AudioFileReader(Directory.GetCurrentDirectory() + @"\ambientePartido.mp3");
                waveOutDeviceAmbient.Init(audioFileReaderAmbient);
                audioFileReaderAmbient.Volume = 0.2f;
                audioFileReaderAmbient.Position = 999999 * 3;
                waveOutDeviceAmbient.Play();
            } 
            else if(i==0)
            {
                waveOutDeviceAmbient.Stop();
            }
        }  
        public static int SelectFromList(List<string> list)
        {
            bool fin = false;
            int index = 0;
            while (!fin)
            {                
                var keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    if (index != list.Count - 1) index++;
                    else index = list.Count - 1;
                    Util.Speak(list[index]);
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    if (index != 0) index--;
                    else index = 0;
                    Util.Speak(list[index]);
                }
                if (keyInfo.Key == ConsoleKey.Spacebar)
                {
                    Util.Speak(list[index]);
                }
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    fin = true;
                    break;
                }
            }
            return index;
        }
        public static bool SiNo(string pregunta)
        {
            bool fin = false;
            ConsoleKeyInfo keyInfo;
            Util.Speak(pregunta+", Pulsa S para sí o cualquier otra tecla para no");
            while (!fin)
            {
                keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.S)
                {
                    break;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        public static string IntToStringSiNo(int i)
        {
            if(i==1)
            {
                return "Sí";
            }
            else
                return "No";
        }
        public static string ReadLine()
        {
            bool fin = false;
            string line = "";
            while (!fin)
            {
                var keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    fin = true;
                    return line;
                }
                line += keyInfo.KeyChar;
                Speak(keyInfo.KeyChar+"");

            }
            return line.ToLower();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManagerFB
{
    public class Patrocinador
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int semanal { get; set; }
        public int anual { get; set; }
    }
}

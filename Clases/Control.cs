﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerFB
{
    public class Control
    {
        public int jornadaActual;
        public int temporadaActual;
        public string clubActual;
        public int primeraPartida;
        public List<int> dorsales = new List<int>();
        
        public Control() //jornadaActual, temporadaActual, clubActual
        {
            var q = DB.query("SELECT * FROM Control");
            List<string> l;
            try
            {
                l = q[0];
            }
            catch(System.ArgumentOutOfRangeException)
            {
                DB.query($"INSERT INTO Control (jornadaActual, temporadaActual, clubActual, primeraPartida) VALUES ('-5', '1', ' ','0');");
                q = DB.query("SELECT * FROM Control"); ;
                l = q[0];
            }
            jornadaActual = int.Parse(l[1]);
            temporadaActual = int.Parse(l[2]);
            clubActual = l[3];
            try
            {
                primeraPartida = int.Parse(l[4]);
            }
            catch (System.FormatException ex)
            {
                primeraPartida = 0;
            }
            q = DB.query($"SELECT dorsal FROM Jugador WHERE club='{clubActual}'");
            foreach (var d in q)
            {
                dorsales.Add(int.Parse(d[0]));
            }

        }

    }
}

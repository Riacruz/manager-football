﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerFB
{
    public class Jugador
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string club { get; set; }

        public int edad { get; set; }
        public int fuerza { get; set; }
        public int velocidad { get; set; }
        public int resistencia { get; set; }
        public int media { get { return (fuerza+velocidad+resistencia)/3; } }
        public int dorsal { get; set; }
        public string puesto { get; set; }
        public int convocado { get; set; }
        public int titular { get; set; }
        public int sueldo { get; set; }
        public int clausula { get; set; }
        public int enTraspaso { get; set; }
        public int contratoRestante { get; set; }
    }
}

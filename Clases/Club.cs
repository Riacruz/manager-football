﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerFB
{
    public class Club
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string estadio { get; set; }
        public int golesAFavor 
        {
            get
            {                
                var q = DB.query($"SELECT golesAFavor FROM Club WHERE id='{DB.GetClub(nombre)}'");
                int i = int.Parse(q[0][0]);
                return i;
            }
                 
        }
        public int golesEnContra
        {
            get
            {
                var q = DB.query($"SELECT golesEnContra FROM Club WHERE id='{DB.GetClub(nombre)}'");
                int i = int.Parse(q[0][0]);
                return i;
            }
        }
        public List<Jugador> lJugadores { get; set; }
        public int media 
        { 
            get 
            {
                int m = 0;
                foreach (var l in lJugadores)
                {
                    m += l.media;
                }
                m = m / lJugadores.Count;
                return m;
            } 
        }
        public int mediaConvocados
        {
            get
            {
                int m = 0;
                var count = 0;
                foreach (var l in lJugadores)
                {
                    if (l.convocado == 1)
                    {
                        m += l.media;
                        count++;
                    }
                }
                m = m / count;
                return m;
            }
        }
        public int presupuesto { get; set; }
        public int precioEntrada { get; set; }
        public int aforo { get; set; }
        public string objetivo { get; set; }
        public string patrocinador { get; set; }
        public List<Resultado> resultados { get; set; }
    }
}

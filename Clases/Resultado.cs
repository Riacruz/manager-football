﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerFB
{
    public class Resultado
    {
        public int id { get; set; }
        public string localNombre { get; set; }
        public string visitanteNombre { get; set; }
        public List<Gol> localGoles { get; set; }
        public List<Gol> visitanteGoles { get; set; }
        public int temporadaId { get; set; }
        public int jornada { get; set; }
    }
}

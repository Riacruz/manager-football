﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ManagerFB
{
    class Temporada
    {
        public string nombre 
        { 
            get 
            {
                var q = DB.query($"SELECT nombre FROM Temporada;");
                return q[new Control().temporadaActual-1][0];
            } 
        }
        public int jornadaActual { get { return new Control().jornadaActual; } }
        public string GetJornada 
        { 
            get
            {
                string str = "";
                int Totaljornadas = (lClubs.Count -1)*2;
                int count = 0;
                List<List<string>> res = new List<List<string>>();
                for (int i = 0; i < calendario.lCalendario.Count; i++)
                {
                    
                    if (i % (lClubs.Count / 2) == 0) count++;
                    if (count == jornadaActual)
                    {
                        var q = DB.query($"SELECT * FROM Calendario WHERE temporadaId='{(int.Parse(new Temporada().nombre))-2019}' AND posicion='{i}'");
                        res.AddRange(q);
                    }
                }
                foreach (var l in res)
                {
                    str += l[0]+"\n";
                }
                return str;
            }
        }
        public Calendario calendario { get { return new Calendario(new Control().temporadaActual); } }     
        public List<Resultado> resultados { get; set; }
        public List<Club> lClubs 
        { //Ojo
            get 
            {
                var clubs = new List<Club>();
                var str = DB.GetIdClubs();
                var split = str.Split(',');
                for (int i=0;i<split.Length-1;i++)
                {   
                    if (string.IsNullOrWhiteSpace(split[i])) continue;
                    clubs.Add(DB.GetClub(int.Parse(split[i])));
                }
                return clubs;
            }  
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManagerFB
{
    public class Globals
    {
        public static string version = "ManagerFB Beta v0.6";
        public static string notas = @"Novedades: Velocidad del lector y configuracion de la música se guardan, implementado lector de voz con la palabra comando,
        .
        Gracias por leer estas notas.";
        public static string[] ayuda = { "- Ayuda: Muestra esta ayuda","- Salir: Sale del juego","- Notas: Notas del autor sobre la versión del juego",
            "- Velocidad numero: Velocidad del lector de texto (del 1 al 4). Ejemplo: velocidad 2",
            "- musica numero: Activar o desactivar música de fondo. Musica 0 la apaga, musica 1 la enciende"};
        public static string[] ayudaMenuJugar = { 
        "- Jugar: Juega la siguiente jornada mostrando resultados y goleadores de los equipos",
        "- Jugar solo o j: Juega la siguiente jornada mostrando sólo el partido de tu club", 
        "- Clasificación o tabla: Muestra la tabla de clasificación actual",
        "- Clubs: Listado de nombres de los clubes actuales",
        "- Jornada: Muestra la jornada en curso", 
        "- Resultados: Muestra los resultados de la última jornada jugada",
        "- Resultados NumeroJornada: Muestra los resultados del numero de jornada elegido. Ejemplo: resultados 2",
        "- Plantilla NúmeroDeDorsal: Muestra las características del jugador de tu club por su número de dorsal. Ejemplo: plantilla 1.",
        "- Jugadores o plantilla: Muestra un listado de los nombres de los jugadores de tu club actual.",
        "- Jugadores NombreDeClub: Muestra un listado de los nombres de los jugadores del club. Ejemplo: jugadores Estrella CF",
        "- Jugadores NombreDeClub media: Muestra un listado de los nombres de los jugadores del club ordenados por media. Ejemplo: jugadores cerruda media",
        "- Jugadores edad NúmeroDeAños: Muestra un listado de los jugadores más viejos hasta la edad elegida. Ejemplo: jugadores edad 40",
        "- Convocados: Muestra un listado de los nombres de los jugadores convocados de tu club actual.",
        "- Convocados NombreDeClub: Muestra un listado de los nombres de los jugadores convocados del club. Ejemplo: convocados cerruda",
        "- Titulares: Muestra un listado de los nombres de los jugadores titulares de tu club actual.",
        "- Titulares NombreDeClub: Muestra un listado de los nombres de los jugadores titulares del club. Ejemplo: titulares cerruda",
        "- Suplentes: Muestra un listado de los nombres de los jugadores suplentes de tu club actual.",
        "- Suplentes NombreDeClub: Muestra un listado de los nombres de los jugadores suplentes del club. Ejemplo: suplentes cerruda",
        "- Club: Muestra los datos de tu club actual.",
        "- Club NombreDeClub: Muestra los datos de tal club. Ejemplo: club Estrella CF",
        "- Posicion: Muestra la posición en la clasificación de tu club actual. Ejemplo: club Estrella CF",
        "- Jugador NombreDeJugador: Muestra las características del jugador. Ejemplo: jugador Marko",
        "- Jugador NúmeroDeDorsal NombreDeClub: Muestra las características del jugador de tal club por su número de dorsal. Ejemplo: jugador 1 cerruda",
        "- Cambio NúmeroDeDorsal NúmeroDeDorsal: Cambia el estado de titular y/o convocado de un jugador por el del otro. Ejemplo: cambio 7 11",
        "- Traspasos: Muestra listado de jugadores en situación de traspaso. Pulsa la tecla O para ofrecer una oferta",
        "- Traspasos media: Muestra listado de jugadores en situación de traspaso ordenados por media. Pulsa la tecla O para ofrecer una oferta",
        "- Ofertas: Muestra listado de las ofertas sin resolver",
        "- Oferta cantidad sueldo NÚmeroDorsal NombreDeClub: Ofrece tal cantidad y tal sueldo a tal jugador de tal club. Ejemplo: oferta 25000 500 1 cerruda",
        "- Pichichi: Ofrece un listado de los máximos goleadores de la temporada.",
        "- Vender NumeroDeDorsal: Pone a la venta el jugador con tal dorsal de tu club o lo quita de la venta si ya estaba. Ejemplo: vender 5",
        "- Titular NumeroDeDorsal: Establece como titular el jugador de tal dorsal. Ejemplo: titular 5",
        "- Suplente NumeroDeDorsal: Establece como suplente el jugador de tal dorsal. Ejemplo: suplente 5",
        "- Desconvocar NumeroDeDorsal: Establece como no convocado el jugador de tal dorsal. Ejemplo: desconvocar 5",
        "- Entrada cantidad: Establece el precio de la entrada a tal cantidad. Ejemplo: entrada 20",
        "- Patrocinador: Ofrece información del patrocinador actual de tu club."
        };
        public static string[] ayudaMenuInicio = { "- Nueva partida: Crea una nueva partida borrando la anterior.", "- Continuar: Continua tu partida actual" };
        public static string[] nombresClubs = { "Estrella CF", "Susana Los Perez", "Cerruda", "Era de Mota", "El Congo United", "Jinamar", "Clipper de Fresa", "Casa Pastores" };
        public static string[] estadios = { "Las Palmitas", "Real Domingo Tres Peos", "Cerruda Stadium", "Los Aberronchos", "Salam Migdagar", "Jinamar Unidos", "El Burbujitas", "El Chapín" };
        public static List<Patrocinador> lPatrocinadores = new List<Patrocinador>() { new Patrocinador() { nombre = "Spar", semanal = 3000, anual = 30000 },
                new Patrocinador() { nombre = "Supermercados Bolaños", semanal = 1000, anual = 10000 },new Patrocinador() { nombre = "Artsfactory.es", semanal = 10000, anual = 100000 }};
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ManagerFB
{
    public class Lector
    {
        public static bool isPretemporada = true;
        public static string LeeInicio(string line)
        {
            if (String.IsNullOrWhiteSpace(line)) return null;
            var split = line.Trim().Split(' ');
            if (split.Length > 1)
            {
                var t = new Temporada();
                var sp = line.Replace(split[0], "");
                sp = sp.Trim();
                var str = "";
                switch (split[0].ToLower())
                {
                    case ("velocidad"):
                        return Velocidad(sp);
                    case ("musica"):
                        return Musica(sp);
                    case ("nueva"):

                        if (sp.Equals("partida"))
                        {
                            //NuevaPartida
                            return DB.NuevaPartida();
                        }
                        else
                            str = "El comando debe ser nueva partida";

                        return str;
                    default:
                        return "El comando " + line + " no es correcto";
                }
            }
            else
            {
                var t = new Temporada();
                switch (line.ToLower())
                {
                    case ("continuar"):
                        if (new Control().primeraPartida != 1)
                        {
                            return "No hay partida en curso";
                        }
                        var str = "Continuando ";
                        if(!isPretemporada)
                            str += "Temporada " + new Temporada().nombre + " Jornada " + new Control().jornadaActual + "\n";
                        else
                            str += "Temporada " + new Temporada().nombre + " Jornada " + (new Control().jornadaActual+6) + " de la pretemporada\n";
                        Util.EstadoDeJuego = Util.state.EnPartida;
                        return str;
                    case ("notas"):
                        return "Notas de " + Globals.version + "\n" + Globals.notas;
                    case ("ayuda"):
                        List<string> list = Globals.ayudaMenuInicio.ToList();
                        list.AddRange(Globals.ayuda.ToList());
                        return Select(list.ToArray(), line, "Menú de ayuda, ");
                    case ("salir"):
                        break;
                    default:
                        return "El comando " + line + " no es correcto";
                }
            }
            return null;
        }
        public static string Lee(string line)
        {
            if (String.IsNullOrWhiteSpace(line)) return null;
            var split = line.Trim().Split(' ');
            if(split.Length>1) // Si hay más de una palabra en el comando
            {
                var t = new Temporada();
                var sp = line.Replace(split[0], "");
                sp = sp.Trim();
                var str = "";
                switch (split[0].ToLower()) 
                {
                    case ("velocidad"):                          
                        return Velocidad(sp);
                    case ("musica"): 
                        return Musica(sp);
                    case ("cambio"):                     
                        return Cambio(sp);
                    case ("cambiar"):                     
                        return Cambio(sp);
                    case ("club"):  //club nombreClub                       
                        var list = LeeClub(sp).Split('\n');
                        if (list.Length < 2) return "No existe club con el nombre " + sp;
                        return Select(list, line);
                    case ("jugadores"):  //jugadores nombreClub  //jugadores edad
                        if (sp.Contains("edad"))
                        {
                            try
                            {
                                return Select(JugadoresEdad(sp), line);
                            }
                            catch (FormatException)
                            {
                                return "El comando es jugadores edad NúmeroDeAños";
                            }
                            catch (System.IndexOutOfRangeException)
                            {
                                return "El comando es jugadores edad NúmeroDeAños";
                            }
                        }
                        list = LeeJugadoresClub(sp).Split('\n');
                        if (list.Length < 2) return "No existe club con el nombre "+sp;
                        Util.Speak("Hay un total de " + (list.Length - 1) + " jugadores");
                        Console.ReadKey();
                        return Select(list, line);
                    case ("titulares"):  //titulares nombreClub                       
                        list = LeeTitularesClub(sp).Split('\n');
                        if (list.Length < 2) return "No existe club con el nombre " + sp;
                        Util.Speak("Hay un total de " + (list.Length - 1) + " jugadores titulares");
                        Console.ReadKey();
                        return Select(list, line);
                    case ("convocados"):  //convocados nombreClub                       
                        list = LeeConvocadosClub(sp).Split('\n');
                        if (list.Length < 2) return "No existe club con el nombre " + sp;
                        Util.Speak("Hay un total de " + (list.Length - 1) + " jugadores convocados");
                        Console.ReadKey();
                        return Select(list, line);
                    case ("suplentes"):  //suplentes nombreClub                       
                        list = LeeSuplentesClub(sp).Split('\n');
                        if (list.Length < 2) return "No existe club con el nombre " + sp;
                        Util.Speak("Hay un total de " + (list.Length - 1) + " jugadores suplentes");
                        Console.ReadKey();
                        return Select(list, line);
                    case ("oferta"):  //oferta cantidad dorsal nombreDeClub
                        return Oferta(sp);
                    case ("traspasos"):
                        if (sp.Trim().Equals("media"))
                        {
                            var spl = LeeTraspasosMedia();
                            return SelectTraspasos(spl, line);
                        }
                        else
                            return "El comando " + line + " es incorrecto";
                    case ("jugar"): //jugar solo
                        if (sp.ToLower().Equals("solo"))
                            if (!isPretemporada) 
                                return Jugar(t, true);
                            else
                                return "En pretemporada usa j o jugar";
                        else
                            return "Los comandos existentes son jugar y jugar solo";
                    case ("vender"):
                        return Vender(sp);
                    case ("titular"):
                        return Titular(sp);
                    case ("suplente"):
                        return Suplente(sp);
                    case ("desconvocar"):
                        return Desconvocar(sp);
                    case ("entrada"):
                        return Entrada(sp);
                    case ("resultados"):
                        return Resultados(sp);
                    case ("jugador"): //jugador NumeroDorsal NombreClub
                        return Jugador(sp);
                    case ("plantilla"): //plantilla NumeroDorsal
                        return Jugador(sp+" "+new Control().clubActual);
                    default:
                        return "El comando "+line+" no es correcto";
                }
            }
            else
            {
                var t = new Temporada();
                switch (line.ToLower())
                {
                    case ("ayuda"):
                        List<string> list = Globals.ayudaMenuJugar.ToList();
                        list.AddRange(Globals.ayuda.ToList());                     
                        return Select(list.ToArray(),line, "Menú de ayuda,");
                    case ("salir"):
                        break;
                    case ("clasificacion"):
                        return Select(Clasificacion().Split('\n'), line, "Temporada " + new Temporada().nombre + " Jornada " + new Control().jornadaActual);
                    case ("tabla"):
                        return Select(Clasificacion().Split('\n'), line, "Temporada " + new Temporada().nombre + " Jornada " + new Control().jornadaActual);
                    case ("jugar"):
                        if (!isPretemporada)
                            return Jugar(t);
                        else
                            return Pretemporada();
                    case ("j"):
                        if (!isPretemporada)
                            return Jugar(t, true);
                        else
                            return Pretemporada();
                    case ("jugardebug"):
                        for (int i=0;i< (new Temporada().lClubs.Count - 1) * 2;i++)
                        {
                            Jugar(t,true);
                        }
                        return null;
                    case ("plantilla"):    
                        var spl = LeeJugadoresClub(new Control().clubActual).Split('\n');
                        Util.Speak("Hay un total de " + (spl.Length-1) + " jugadores en tu plantilla");
                        Console.ReadKey();
                        return Select(spl,line);
                    case ("jugadores"):    
                         spl = LeeJugadoresClub(new Control().clubActual).Split('\n');
                        Util.Speak("Hay un total de " + (spl.Length - 1) + " jugadores");
                        Console.ReadKey();
                        return Select(spl,line);
                    case ("titulares"):    
                        spl = LeeTitularesClub(new Control().clubActual).Split('\n');
                        Util.Speak("Hay un total de " + (spl.Length - 1) + " jugadores titulares");
                        Console.ReadKey();
                        return Select(spl,line);
                    case ("suplentes"):    
                        spl = LeeSuplentesClub(new Control().clubActual).Split('\n');
                        Util.Speak("Hay un total de " + (spl.Length - 1) + " jugadores suplentes");
                        Console.ReadKey();
                        return Select(spl,line);
                    case ("convocados"):    
                        spl = LeeConvocadosClub(new Control().clubActual).Split('\n');
                        Util.Speak("Hay un total de " + (spl.Length - 1) + " jugadores convocados");
                        Console.ReadKey();
                        return Select(spl,line);
                    case ("club"):    
                        spl = LeeClub(new Control().clubActual).Split('\n');
                        return Select(spl,line);
                    case ("pichichi"):
                        var str = "";
                        try { str = Pichichi(); }
                        catch (System.IndexOutOfRangeException)
                        { str = "Aún nadie ha marcado un gol"; }
                        return Select(str.Split('\n'),line);
                    case ("traspasos"):    
                        spl = LeeTraspasos().Split('\n');
                        return SelectTraspasos(spl,line);
                    case ("ofertas"):    
                        spl = Ofertas().Split('\n');
                        return Select(spl,line);
                    case ("notas"):
                        return "Notas de " + Globals.version + "\n"+ Globals.notas;
                    case ("resultados"):
                        return Resultados();
                    case ("jornada"):
                        if (isPretemporada)
                            return "Jornada " +(new Control().jornadaActual+6)+ " de la pretemporada, quedan "+(Math.Abs(new Control().jornadaActual)+1)+" jornadas para el inicio de temporada";
                        else
                        {
                            spl = ("Jornada " + new Control().jornadaActual + "\n" + t.GetJornada).Replace("-", " ").Trim().Split('\n');
                            return Select(spl, line);
                        }
                    case ("clubs"):
                        return Clubs();
                    case ("patrocinador"):
                        try
                        {
                            var c = DB.GetClub(DB.GetClub(new Control().clubActual));
                            var lis = Globals.lPatrocinadores.Select(n => n.nombre).ToList();
                            var patrocinador = Globals.lPatrocinadores[lis.IndexOf(c.patrocinador)];
                            return patrocinador.nombre + ", semanal " + patrocinador.semanal + " euros, anual" + patrocinador.anual + " euros";
                        }
                        catch (System.ArgumentOutOfRangeException)
                        {
                            return "Sin patrocinador";
                        }
                    case ("posicion"):
                        var club = DB.GetClub(DB.GetClub(new Control().clubActual));
                        return "Puesto "+Posicion(club);
                    default:
                        return "El comando " + line + " no es correcto";
                }
            }
            return null;
        }    
        private static string Ofertas()
        {
            string str = "";
            var q = DB.query("SELECT * FROM Oferta");
            foreach (var o in q)
            {// idJugador, cantidad, jornada
                var jug = DB.query($"SELECT * FROM Jugador WHERE id='{o[1]}'");
                str += o[2] + " euros ofrecidos al club " + jug[0][1] + " por el jugador " + jug[0][0] + " con sueldo de "+o[4]+" en la jornada " + o[3]+"\n";
            }
            return str;
        }
        private static string Oferta(string sp)
        {
            var split = sp.Split(' ');
            try
            {
                var cantidad = int.Parse(split[0]);
                var sueldo = int.Parse(split[1]);
                var dorsal = int.Parse(split[2]);
                var nombreDeClub = sp.Trim().Replace(split[0], "");                
                nombreDeClub = nombreDeClub.Trim().Replace(split[1], "").Trim();
                nombreDeClub = nombreDeClub.Trim().Replace(split[2], "").Trim();
                if (nombreDeClub.Equals(new Control().clubActual, StringComparison.InvariantCultureIgnoreCase)) return "Estás haciendo una oferta sin sentido a tu propio club";
                var lJug = DB.GetJugadoresClub(nombreDeClub);
                if (lJug.Count == 0)
                    return "No coincide ningún club con el nombre: " + nombreDeClub;
                var ju = lJug.Select(n => n).Where(n => n.dorsal == dorsal).ToList();
                var l = ju[0];
                var q = DB.query($"SELECT * FROM Oferta WHERE idJugador='{l.id}'");
                if (q.Count > 0)
                    return "No puedes realizar más ofertas por este jugador hasta que se decida la anterior";
                if (ju.Count == 0)
                    return "No coincide jugador del " + nombreDeClub + " con dorsal " + dorsal;
                DB.query($"INSERT INTO Oferta (idJugador,cantidad,jornada,sueldo) VALUES ('{l.id}','{cantidad}','{new Control().jornadaActual}','{sueldo}');");
                return "Oferta de " + cantidad + " euros con sueldo de " +sueldo+" euros por " + l.nombre + " realizada";
            }
            catch (FormatException ex)
            {
                return "El formato del comando es oferta cantidad dorsal nómbreDeClub";
            }
        }
        private static string LeeTraspasos()
        {
            var str = "";
            var q = DB.query($"SELECT * FROM Jugador WHERE enTraspaso='1' AND club!='{new Control().clubActual}'");
            foreach (var f in q)
            {
                int media = (int.Parse(f[3]) + int.Parse(f[4]) + int.Parse(f[5])) / 3;
                str += f[0] +", "+ f[8] +" dorsal " + f[7] + " del club '" + f[1] + "', Media "+media+", Precio " + f[12] + " euros , Sueldo " + f[11] + " euros\n";
            }
            return str;
        }     
        private static string[] LeeTraspasosMedia()
        {
            var q = DB.query("SELECT * FROM Jugador WHERE enTraspaso='1'");
            List<string> lStr = new List<string>();
            List<int> lInt = new List<int>();
            foreach (var f in q)
            {
                int media = (int.Parse(f[3]) + int.Parse(f[4]) + int.Parse(f[5])) / 3;
                var str = f[0] +", "+ f[8] +" dorsal " + f[7] + " del club '" + f[1] + "', Media "+media+", Precio " + f[12] + " euros , Sueldo " + f[11] + " euros\n";
                lStr.Add(str);
                lInt.Add(media);
            }
            var letras = lStr.ToArray();
            var index = lInt.ToArray();
            Array.Sort(index, letras);
            Array.Reverse(letras);
            return letras;
        }
        private static string Select(string[] list, string line, string preText = "")
        {
            if (list.Length < 2) return "No hay "+line;
            Util.Speak(preText + ", "+list[0]+", Usa las teclas para desplazarte Enter para salir");
            Util.SelectFromList(list.ToList());
            return "Saliendo de " + line;
        }
        private static int SelectWithOptions(string[] list, string preText = "")
        {
            Util.Speak(preText + " Usa las teclas para desplazarte");
            int index = Util.SelectFromList(list.ToList());
            return index;
        }
        private static string SelectTraspasos(string[] list, string line, string preText = "")
        {
            bool inOferta = false;
            Util.Speak(preText + ", "+list[0]+", Usa las teclas para desplazarte Enter para salir");
            bool fin = false;
            int index = 0;
            while (!fin)
            {
                var keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    if (index != list.Length - 1) index++;
                    else index = list.Length - 1;
                    Util.Speak(list[index]);
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    if (index != 0) index--;
                    else index = 0;
                    Util.Speak(list[index]);
                }
                if (keyInfo.Key == ConsoleKey.Spacebar)
                {
                    Util.Speak(list[index]);
                }
                if (keyInfo.Key == ConsoleKey.O)
                {
                    inOferta = true;
                    Util.Speak("Introduce la cantidad de cláusula ofrecida y pulsa enter, Solo enter para volver a traspasos");
                    while (!fin)
                    {
                        var linea = Util.ReadLine();
                        if (String.IsNullOrWhiteSpace(linea))
                        {
                           // inOferta = false;
                            Util.Speak(list[index]);
                            break;
                        }
                        int cant = 0;
                        int sueldo = 0;
                        try
                        {
                            cant = int.Parse(linea);
                            if(cant<1)
                            {
                                Util.Speak("Debe ser una cantidad mayor que 0");
                                continue;
                            }
                            Util.Speak("Introduce un sueldo y pulsa enter, Solo enter para volver a cantidad de cláusula");
                            while(!fin)
                            {
                                linea = Util.ReadLine();
                                if (String.IsNullOrWhiteSpace(linea))
                                {
                                    inOferta = false;
                                    Util.Speak("Introduce la cantidad de cláusula ofrecida y pulsa enter, Solo enter para volver a traspasos");
                                    break;
                                }
                                try
                                {
                                    sueldo = int.Parse(linea);
                                    if (sueldo < 1)
                                    {
                                        Util.Speak("Debe ser una cantidad mayor que 0");
                                        continue;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                catch (FormatException)
                                {
                                    Util.Speak("El formato de la cantidad no es correcto");
                                    continue;
                                }
                            }
                            var spl = list[index].Split(new string[] { "dorsal " }, StringSplitOptions.RemoveEmptyEntries);
                            spl = spl[1].Split(' ');
                            var dorsal = spl[0].Trim();
                            spl = list[index].Split('\'');
                            var club = spl[1].Trim();
                            var sp = cant+" "+sueldo+" "+dorsal+" "+club;
                            //Console.WriteLine(sp);
                            if (!inOferta)
                            {
                                inOferta = true;
                                continue;
                            }
                            Util.Speak(Oferta(sp));
                            inOferta = false;
                            break;
                        }
                        catch (FormatException)
                        {
                            Util.Speak("El formato de la cantidad no es correcto");
                            continue;
                        }

                    }
                }
                if (keyInfo.Key == ConsoleKey.Enter && !inOferta)
                {
                    fin = true;
                    return "Saliendo de traspasos";
                }
            }
            return "";
        }
        private static string LeeClub(string nombre)
        { 
            string str = "";
            try
            {
                var n = nombre;
                var club = DB.GetClub(DB.GetClub(n));
                str = "Nombre " + club.nombre + "\n";
                str += "Estadio " + club.estadio + "\n";
                str += "Aforo " + club.aforo + " personas\n";
                str += "Precio de entrada " + club.precioEntrada + " euros\n";
                str += "Presupuesto " + club.presupuesto + " euros \n";
                var clubs = new Temporada().lClubs;
                var sueldos = 0;
                foreach (var j in club.lJugadores)
                {
                    sueldos += j.sueldo;
                }
                str += "Gasto en sueldos por jornada " + sueldos + " euros \n";
                str += "Patrocinador " + club.patrocinador + "\n";
                str += "Media general " + club.media + "\n";
                str += "Media convocados " + club.mediaConvocados + "\n";
                str += "Objetivo " + club.objetivo + "\n";
                str += Clasificacion(club);
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                str = "No coincide ningún club con el nombre: " + nombre;
            }
            return str;
        }
        private static string LeeSuplentesClub(string club)
        { string str = "";
            try
            {
                var lJug = DB.GetJugadoresClub(club);
                foreach (var l in lJug)
                {
                    if (l.titular == 0 && l.convocado == 1)
                    {
                        str += l.dorsal + " ";
                        str += l.nombre + ", ";
                        str += l.puesto + ", ";
                        str += "Media " + l.media + "\n";
                    }
                }
                
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                str = "No coincide ningún club con el nombre: " + club;
            }
            return str;
        }
        private static string LeeConvocadosClub(string club)
        { string str = "";
            try
            {
                var lJug = DB.GetJugadoresClub(club);
                foreach (var l in lJug)
                {
                    if (l.convocado != 1) continue;
                    str += l.dorsal + " ";
                    str += l.nombre + ", ";
                    str += l.puesto + ", ";
                    str += "Media " + l.media + "\n";
                }
                
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                str = "No coincide ningún club con el nombre: " + club;
            }
            return str;
        }
        private static string LeeTitularesClub(string club)
        { string str = "";
            try
            {
                var lJug = DB.GetJugadoresClub(club);
                foreach (var l in lJug)
                {
                    if (l.titular != 1) continue;
                    str += l.dorsal + " ";
                    str += l.nombre + ", ";
                    str += l.puesto + ", ";
                    str += "Media " + l.media + "\n";
                }
                
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                str = "No coincide ningún club con el nombre: " + club;
            }
            return str;
        }
        private static string LeeJugadoresClub(string club, bool media=false)
        { 
            string str = "";
            if(club.Contains(" media"))
            {
                media = true;
            }
            try
            {
                if (media) club = club.Replace(" media","");
                var lJug = DB.GetJugadoresClub(club).ToArray();
                var listMedias = lJug.Select(n => n.media).ToArray();
                if (media)
                {
                    Array.Sort(listMedias, lJug);
                    Array.Reverse(lJug);
                }
                foreach (var l in lJug)
                {
                    var estado = "";
                    if (l.titular == 1)
                        estado = "Titular";
                    else if (l.titular == 0 && l.convocado == 1)
                        estado = "Suplente";
                    else
                        estado = "No convocado";
                    str += l.dorsal + " ";
                    str += l.nombre + ", ";
                    str += l.puesto + " ";
                    str += estado + ", ";
                    str += "Media " + l.media + ", ";
                    str += "Edad " + l.edad + ", ";
                    str += "En traspaso " + Util.IntToStringSiNo(l.enTraspaso) + ", ";
                    str += "Contrato restante " + l.contratoRestante + " años, ";
                    str += "Sueldo " + l.sueldo + " euros, ";
                    str += "Claúsula de rescición " + l.clausula + " euros \n";
                }
                
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                str = "No coincide ningún club con el nombre: " + club;
            }
            return str;
        }
        private static string LeeJugador(int index, string club)
        { 
            string str = "";
            try
            {
                var lJug = DB.GetJugadoresClub(club);
                if(lJug.Count==0)
                {
                    str = "No coincide ningún club con el nombre: " + club;
                    return str;

                }
                var ju = lJug.Select(n => n).Where(n => n.dorsal == index).ToList();
                // var l = lJug[index-1];
                var l = ju[0];
                str += "Nombre " + l.nombre + "\n";
                str += "Club " + l.club + "\n";
                str += "Edad " + l.edad + "\n";
                str += "Fuerza " + l.fuerza + "\n";
                str += "Velocidad " + l.velocidad + "\n";
                str += "Resistencia " + l.resistencia + "\n";
                str += "Media " + l.media + "\n";
                str += "Dorsal " + l.dorsal + "\n";
                str += "Posición " + l.puesto + "\n";
                str += "Convocado " + Util.IntToStringSiNo(l.convocado) + "\n";
                str += "Titular " + Util.IntToStringSiNo(l.titular) + "\n";
                str += "Sueldo " + l.sueldo + " euros\n";
                str += "Cláusula de rescición " + l.clausula + " euros\n";
                str += "En traspaso " + Util.IntToStringSiNo(l.enTraspaso) + "\n";
                str += "Contrato restante " + l.contratoRestante + " años\n";
                var q = DB.query($"SELECT partido FROM Gol WHERE idJudagor='{l.id}' AND temporadaId='{new Control().temporadaActual}'");
                var count = 0;
                foreach (var v in q)
                {
                    count++;
                }
                str += "Goles " + count + "";
                return str;
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                str = "Error al insertar número de jugador o club";
                return str;
            }
            str = index + " no existe como jugador del " + club;
            return str;
        }
        private static string LeeJugador(string nombre)
        { 
            string str = "";
            try
            {
                var temporada = new Temporada();
                foreach (var c in temporada.lClubs)
                {
                    var lJug = DB.GetJugadoresClub(c.nombre);
                    if (lJug.Count == 0)
                    {
                        str = "No coincide ningún club con el nombre: " + c.nombre;
                        return str;

                    }
                    foreach (var l in lJug)
                    {
                        // Util.Speak(l.nombre.ToLower()+" "+nombre.ToLower());
                        if (l.nombre.ToLower().Equals(nombre.ToLower()))
                        {

                            str += "Nombre " + l.nombre + "\n";
                            str += "Club " + l.club + "\n";
                            str += "Edad " + l.edad + "\n";
                            str += "Fuerza " + l.fuerza + "\n";
                            str += "Velocidad " + l.velocidad + "\n";
                            str += "Resistencia " + l.resistencia + "\n";
                            str += "Media " + l.media + "\n";
                            str += "Dorsal " + l.dorsal + "\n";
                            str += "Posición " + l.puesto + "\n";
                            str += "Convocado " + Util.IntToStringSiNo(l.convocado) + "\n";
                            str += "Titular " + Util.IntToStringSiNo(l.titular) + "\n";
                            str += "Sueldo " + l.sueldo + " euros\n";
                            str += "Cláusula de rescición " + l.clausula + " euros\n";
                            str += "En traspaso " + Util.IntToStringSiNo(l.enTraspaso) + "\n";
                            str += "Contrato restante " + l.contratoRestante + " años\n";
                            var q = DB.query($"SELECT partido FROM Gol WHERE idJudagor='{l.id}' AND temporadaId='{new Control().temporadaActual}'");
                            var count = 0;
                            foreach (var v in q)
                            {
                                count++;
                            }
                            str += "Goles " + count + "";
                            //el aumento de jugadores estaba aqui 
                            return str;
                        }
                    }
                } 
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                str = "No hay clubs";
                return str;
            }
            str = nombre + " no existe como jugador";
            return str;
        }
        private static string Velocidad(string sp)
        {
            var str = "";
            try
            {
                Util.SpeakSpeed(int.Parse(sp));
                str = "Velocidad del texto cambiada";
            }
            catch (System.FormatException ex)
            {
                str = "El comando debe ser velocidad numero";
            }
            return str;
        }
        private static string Musica(string sp)
        {
            var str = "";
            try
            {
                Util.PlayMusic(int.Parse(sp), false);
                str = "Música cambiada a " + sp;
            }
            catch (System.FormatException ex)
            {
                str = "El comando debe ser música numero";
            }
            return str;
        }
        private static string Clasificacion(Club club)
        {
            var q = DB.query($"SELECT * FROM Resultado WHERE temporadaId='{new Control().temporadaActual}'");
            var str = "";
            int puntosClubs = 0;
           // str += "Temporada " + new Temporada().nombre + " Jornada " + new Control().jornadaActual+"\n";
            foreach (var r in q)
            { //local, visitante, localGoles, visitanteGoles, temporadaId
                  int localGoles = int.Parse(r[2]);
                    int visitanteGoles = int.Parse(r[3]);
                    if (r[0].ToLower().Equals(club.nombre.ToLower()) && localGoles > visitanteGoles)
                    {
                        puntosClubs = puntosClubs + 3;
                    }
                    else if (r[1].ToLower().Equals(club.nombre.ToLower()) && localGoles < visitanteGoles)
                    {
                        puntosClubs = puntosClubs + 3;
                    }
                    else if (r[0].ToLower().Equals(club.nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs = puntosClubs + 1;
                    }
                    else if (r[1].ToLower().Equals(club.nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs = puntosClubs + 1;
                    }
                
            }
            str += puntosClubs+" puntos, "+club.golesAFavor + " goles a favor, " + club.golesEnContra + " goles en contra" + "\n";            
            return str.Trim();
        }
        private static string Clasificacion()
        {
            var q = DB.query($"SELECT * FROM Resultado WHERE temporadaId='{new Control().temporadaActual}'");
            var str = "";
            Club[] clubs = new Temporada().lClubs.ToArray();
            int[] puntosClubs = new int[clubs.Length];
            //str += "Temporada " + new Temporada().nombre + " Jornada " + new Control().jornadaActual+"\n";
            foreach (var r in q)
            { //local, visitante, localGoles, visitanteGoles, temporadaId
                for (int i = 0; i < clubs.Length; i++)
                {
                    int localGoles = int.Parse(r[2]);
                    int visitanteGoles = int.Parse(r[3]);
                    if (r[0].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles > visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 3;
                    }
                    else if (r[1].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles < visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 3;
                    }
                    else if (r[0].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 1;
                    }
                    else if (r[1].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 1;
                    }
                }
            }
            Array.Sort(puntosClubs, clubs);
            Array.Reverse(clubs);
            Array.Reverse(puntosClubs);
            for (int i = 0; i < clubs.Length; i++)
            {
                str += (i +1)+"º "+clubs[i].nombre + ", "+ puntosClubs[i]+" puntos, "+clubs[i].golesAFavor + " goles a favor, " + clubs[i].golesEnContra + " goles en contra" + "\n";
            }
            return str.Trim();
        }
        private static string Posicion(Club club)
        {
            var q = DB.query($"SELECT * FROM Resultado WHERE temporadaId='{new Control().temporadaActual}'");
            Club[] clubs = new Temporada().lClubs.ToArray();
            int[] puntosClubs = new int[clubs.Length];
            var str = "";
            foreach (var r in q)
            { //local, visitante, localGoles, visitanteGoles, temporadaId
                for (int i = 0; i < clubs.Length; i++)
                {
                    int localGoles = int.Parse(r[2]);
                    int visitanteGoles = int.Parse(r[3]);
                    if (r[0].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles > visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 3;
                    }
                    else if (r[1].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles < visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 3;
                    }
                    else if (r[0].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 1;
                    }
                    else if (r[1].ToLower().Equals(clubs[i].nombre.ToLower()) && localGoles == visitanteGoles)
                    {
                        puntosClubs[i] = puntosClubs[i] + 1;
                    }
                }
            }
            Array.Sort(puntosClubs, clubs);
            Array.Reverse(clubs);
            Array.Reverse(puntosClubs);
            for (int i = 0; i < clubs.Length; i++)
            {
                if (club.nombre.Equals(clubs[i].nombre))
                    str += (i + 1).ToString();
            }
            return str;
        }
        private static string Resultados(string sp)
        {
            var st = "";
            try
            {
                var q = DB.query($"SELECT * FROM Resultado WHERE jornada='{int.Parse(sp)}'");
                foreach (var v in q)
                {
                    st += v[0] + " " + v[2] + " " + v[1] + " " + v[3] + "\n";
                }
                if (q.Count == 0) return "No hay resultados para la jornada " + sp;
            }
            catch (System.FormatException ex)
            {
                st = "El comando debe ser resultados numero";
            }
            return st;
        }
        private static string Resultados()
        {
            string[] spl;
            var q = DB.query($"SELECT * FROM Resultado WHERE jornada='{new Control().jornadaActual - 1}'");
            var st = "";
            foreach (var v in q)
            {
                st += v[0] + " " + v[2] + " " + v[1] + " " + v[3] + "\n";
            }
            if (q.Count == 0) return "No hay resultados";
            Util.Speak("Usa las teclas para desplazarte, Enter para salir.");
            spl = st.Trim().Split('\n');
            Util.SelectFromList(spl.ToList());
            return "Saliendo de resultados";
        }
        private static string Vender(string sp)
        {
            try
            {
                var club = DB.GetClub(DB.GetClub(new Control().clubActual));
                var list = club.lJugadores.Select(n => n).Where(n => n.enTraspaso == 1).ToList();
                int count = club.lJugadores.Count-list.Count;
                
                int dorsal = int.Parse(sp);
                var jug = DB.GetJugadorDorsal(dorsal);
                if (jug.enTraspaso == 1)
                {
                    DB.query($"UPDATE Jugador SET enTraspaso='0' WHERE id='{jug.id}'");
                    return "El jugador "+jug.dorsal+" "+jug.nombre+" ya no está en venta";
                }
                else
                {
                    if (count <= 17) return "El club debe terner mínimo 17 jugadores, no puedes poner más jugadores en traspaso";
                    DB.query($"UPDATE Jugador SET enTraspaso='1' WHERE id='{jug.id}'");
                    return "El jugador " + jug.dorsal + " " + jug.nombre + " ha sido puesto en venta";
                }
                
            }
            catch (System.ArgumentOutOfRangeException)
            {
                return "No existe jugador con ese dorsal";
            }
            catch (System.FormatException)
            {
                return "El formato es vender NúmeroDeDorsal";
            }
        }
        private static string Titular(string sp)
        {
            try
            {
                int dorsal = int.Parse(sp);
                var jug = DB.GetJugadorDorsal(dorsal);
                if (jug.titular == 0)
                {
                    var list = DB.GetJugadoresClub(new Control().clubActual);
                    var titulares = list.Select(n => n).Where(n => n.titular == 1).ToList();
                    if (titulares.Count == 11)
                    {
                        return "Solo puedes tener 11 jugadores titulares";
                    }
                    DB.query($"UPDATE Jugador SET titular='1', convocado='1' WHERE id='{jug.id}'");
                    return "El jugador "+jug.dorsal+" "+jug.nombre+" se ha incluido en el 11 titular";
                }
                else 
                {
                    return "El jugador " + jug.dorsal + " " + jug.nombre + " ya es titular";
                }
            }
            catch (System.ArgumentOutOfRangeException)
            {
                return "No existe jugador con ese dorsal";
            }
            catch (System.FormatException)
            {
                return "El formato es titular NúmeroDeDorsal";
            }
        }
        private static string Suplente(string sp)
        {
            try
            {
                int dorsal = int.Parse(sp);
                var jug = DB.GetJugadorDorsal(dorsal);
                if (jug.convocado == 0)
                {
                    var list = DB.GetJugadoresClub(new Control().clubActual);
                    var convocados = list.Select(n => n.convocado).ToList();
                    if (convocados.Count == 17)
                    {
                        return "Solo puedes tener 17 jugadores convocados";
                    }
                    DB.query($"UPDATE Jugador SET titular='0', convocado='1' WHERE id='{jug.id}'");
                    return "El jugador " + jug.dorsal + " " + jug.nombre + " ha cambiado su estado a suplente";
                }
                else
                {
                    return "El jugador " + jug.dorsal + " " + jug.nombre + " ya es suplente";
                }
            }
            catch (System.ArgumentOutOfRangeException)
            {
                return "No existe jugador con ese dorsal";
            }
            catch (System.FormatException)
            {
                return "El formato es suplente NúmeroDeDorsal";
            }
        }
        private static string Desconvocar(string sp)
        {
            try
            {
                int dorsal = int.Parse(sp);
                var jug = DB.GetJugadorDorsal(dorsal);
                if (jug.convocado == 1)
                {
                    DB.query($"UPDATE Jugador SET titular='0', convocado='0' WHERE id='{jug.id}'");
                    return "El jugador " + jug.dorsal + " " + jug.nombre + " ha cambiado su estado a no convocado";
                }
                else
                {
                    return "El jugador " + jug.dorsal + " " + jug.nombre + " no estaba convocado";
                }
            }
            catch (System.ArgumentOutOfRangeException)
            {
                return "No existe jugador con ese dorsal";
            }
            catch (System.FormatException)
            {
                return "El formato es suplente NúmeroDeDorsal";
            }
        }
        private static string Clubs()
        {
            var t = new Temporada();
            var str = "";
            foreach (var l in t.lClubs)
            {
                str += l.nombre + "\n";
            }
            Util.Speak("Usa las teclas para desplazarte, Enter para salir.");
            var spl = str.Trim().Split('\n');
            Util.SelectFromList(spl.ToList());
            return "Saliendo de clubs";
        }
        private static string Jugador(string sp)
        {
            var spl = sp.Split(' ');
            string[] list;
            if (spl.Length > 1)
            {
                
                try
                {
                    // str = LeeJugador(int.Parse(spl[0]), sp.Replace(spl[0],""));
                    list = LeeJugador(int.Parse(spl[0]), sp.Replace(spl[0], "")).Split('\n');
                    if (list.Length < 2) return "No existe club con el nombre " + sp;
                    Util.Speak(list[0]+", Usa las teclas para desplazarte. Enter para salir.");
                    spl = list;
                    Util.SelectFromList(spl.ToList());
                    return "Saliendo de jugador " + sp;
                }
                catch (System.FormatException ex)
                {
                    return "El formato del comando es: jugador númeroDeJugador nombreDeClub";
                }
            }
            else
            {
                // str = LeeJugador(sp);
                list = LeeJugador(sp).Split('\n');
                if (list.Length < 2) return "No existe club con el nombre " + sp;
                Util.Speak("Usa las teclas para desplazarte. Enter para salir.");
                spl = list;
                Util.SelectFromList(spl.ToList());
                return "Saliendo de jugador " + sp;

            }
        }
        private static string Cambio(string sp)
        {
            sp = sp.Replace(" por", "");
            var split = sp.Split(' ');
            if (split.Length < 2) return "El comando debe ser cambio numeroDorsal numeroDorsal";
            try
            {
                int jug = int.Parse(split[0]);
                int newJug = int.Parse(split[1]);
                try
                {
                    Jugador jugador = DB.GetJugadorDorsal(jug);
                    Jugador newJugador = DB.GetJugadorDorsal(newJug);
                    Console.WriteLine(jugador.dorsal + " " + newJugador.dorsal);
                    DB.query($"UPDATE Jugador SET titular='{newJugador.titular}', convocado='{newJugador.convocado}' WHERE dorsal='{jugador.dorsal}' AND club='{new Control().clubActual}'");
                    DB.query($"UPDATE Jugador SET titular='{jugador.titular}', convocado='{jugador.convocado}' WHERE dorsal='{newJugador.dorsal}' AND club='{new Control().clubActual}'");
                }
                catch (System.ArgumentOutOfRangeException ex)
                {
                    return "Debes poner dorsales existentes";
                }
            }
            catch (FormatException ex)
            {
                return "El comando debe ser cambio numeroDorsal numeroDorsal";
            }
            return "Cambio realizado";
        }
        private static string CompruebaOfertas()
        {
            string str = "";
            var q = DB.query($"SELECT * FROM Oferta");
            foreach (var o in q)
            {// idJugador, cantidad, jornada, sueldo
                var jug = DB.query($"SELECT * FROM Jugador WHERE id='{o[1]}'");
                
                var r = new Random();
                bool theIf = int.Parse(o[3]) + r.Next(1, 3) <= new Control().jornadaActual;
                if(Lector.isPretemporada) theIf = int.Parse(o[3])-1  <= new Control().jornadaActual;
                if (theIf)
                {//si la cantidad ofrecida es igual o superior a la cantidad pedida && sueldo de jugador ofrecido es igual o superior a sueldoActual && está en traspaso => true
                    bool traspasoOK = false;
                    string clubOferta = jug[0][1];
                    int cantidadOfrecida = int.Parse(o[2]);
                    int cantidadPedida = int.Parse(jug[0][12]);
                    int sueldoOfrecido = int.Parse(o[4]);
                    int sueldoActual = int.Parse(jug[0][11]);
                    int enTraspaso = int.Parse(jug[0][13]);
                    int dorsal = int.Parse(jug[0][7]);
                    int id = int.Parse(jug[0][6]);
                    int clubJugadorMedia = 0;
                    int clubActualMedia = 0;

                    Club clubJugador;
                    Club clubActual;
                    if (jug[0][1].Equals("Sin club"))
                    {
                        clubActualMedia = 100;
                        clubJugadorMedia = 0;
                    } else
                    {
                        clubJugador = DB.GetClub(DB.GetClub(jug[0][1]));
                        clubActual = DB.GetClub(DB.GetClub(new Control().clubActual));
                        clubJugadorMedia = clubJugador.media;
                        clubActualMedia = clubActual.media;
                    }

                    if (cantidadOfrecida>=cantidadPedida && sueldoOfrecido >= sueldoActual && enTraspaso==1)
                    {
                        traspasoOK = true;
                    }
                    else if (cantidadOfrecida >= cantidadPedida && sueldoOfrecido*1.1f >= sueldoActual && enTraspaso == 1)
                    {
                        traspasoOK = true;
                    }
                    else
                    {
                        if(clubActualMedia > clubJugadorMedia && cantidadOfrecida*1.1f >= cantidadPedida && sueldoOfrecido*1.1f >= sueldoActual && enTraspaso==1)
                        {
                            traspasoOK = true;
                        }
                    }
                    if(enTraspaso==0 && cantidadOfrecida >= cantidadPedida*3 && sueldoOfrecido >= sueldoActual*2)
                    {
                        traspasoOK = true;
                    }
                    if (traspasoOK)
                    {
                        while (new Control().dorsales.Contains(dorsal))
                        {
                            dorsal = r.Next(1, 100);
                        }
                        var nombreClubActual = new Control().clubActual;
                        DB.query($"UPDATE Jugador SET club='{nombreClubActual}', dorsal='{dorsal}', enTraspaso='0'" +
                            $", convocado='0', titular='0', contratoRestante='4' WHERE id='{id}'");
                        q = DB.query($"SELECT presupuesto FROM Club WHERE nombre='{nombreClubActual}'");
                        int presupuesto = int.Parse(q[0][0]);
                        DB.query($"UPDATE Club SET presupuesto='{presupuesto - cantidadOfrecida}' WHERE nombre='{nombreClubActual}'");
                        q = DB.query($"SELECT presupuesto FROM Club WHERE nombre='{clubOferta}'");
                        if (!jug[0][1].Equals("Sin club"))
                        {
                            presupuesto = int.Parse(q[0][0]);
                            DB.query($"UPDATE Club SET presupuesto='{presupuesto + cantidadOfrecida}' WHERE nombre='{clubOferta}'");
                        }
                        str += jug[0][1] + " ha aceptado el traspaso del jugador " + jug[0][0] + "\n";
                        continue;
                        //Traspaso OK
                    }
                    else
                    {
                        str += jug[0][1] + " ha rechazado el traspaso del jugador " + jug[0][0] + "\n";
                        DB.query($"DELETE FROM Oferta WHERE id='{o[0]}'");
                        //Traspaso FALSE
                    }
                }
            }
            return str;
        }
        private static string Pichichi()
        {
            var q = DB.query($"SELECT * FROM Gol WHERE temporadaId='{new Control().temporadaActual}'");
            var str = "";
            List<int> lIds = new List<int>();
            List<int> lCount = new List<int>();
            foreach (var g in q)
            {
                var idJugador = int.Parse(g[1]);
                if (!lIds.Contains(idJugador))
                {
                    lIds.Add(idJugador);
                }
            }
            foreach (var i in lIds)
            {
                q = DB.query($"SELECT partido FROM Gol WHERE idJudagor='{i}' AND temporadaId='{new Control().temporadaActual}'");
                var count = 0;
                foreach (var v in q)
                {
                    count++;
                }
               // Console.WriteLine("Goles de " + DB.GetJugadorId(i).nombre + count + "");
                lCount.Add(count);
            }
            int[] arr = lIds.ToArray();
            int[] arrCount = lCount.ToArray();
            Array.Sort(arrCount, arr);
            Array.Reverse(arr);
            Array.Reverse(arrCount);
            for (int i = 0; i < arr.Length; i++)
            {
                var jug = DB.GetJugadorId(arr[i]);
                str += (i+1) + " " + jug.nombre + " del club " + jug.club + " con " + arrCount[i] + " goles, dorsal "+jug.dorsal+" "+jug.puesto+"\n";
            }            
            return str;
        }
        private static void RestaSueldosCompruebaPatrocinadores()
        {
            var clubs = new Temporada().lClubs;
            foreach (var c in clubs)
            {
                var sueldos = 0;
                foreach (var j in c.lJugadores)
                {
                    sueldos += j.sueldo;
                }
                if(Util.clubsLocalesUltimaJornada.Contains(c))
                {
                    c.presupuesto = c.presupuesto - sueldos + Util.aforosUltimaJornada[Util.clubsLocalesUltimaJornada.IndexOf(c)]*c.precioEntrada;
                }
                c.presupuesto = c.presupuesto - sueldos;
                DB.query($"UPDATE Club SET presupuesto='{c.presupuesto}' WHERE id='{c.id}'");
            }
            CompruebaPatrocinadores();
        }
        private static void CompruebaPatrocinadores(bool nuevoPatrocinador=false)
        {
            var clubs = new Temporada().lClubs;
            //Comprueba patrocinadores
            var r = new Random();
            var club = DB.GetClub(DB.GetClub(new Control().clubActual));
            bool theIf = int.Parse(Posicion(club)) > (clubs.Count / 2) && club.patrocinador.Equals("Sin patrocinador");
            if (nuevoPatrocinador) theIf = int.Parse(Posicion(club)) > (clubs.Count / 2);
            if (theIf)
            {
                var patrocinador = Globals.lPatrocinadores[r.Next(0, Globals.lPatrocinadores.Count)];
                Util.Speak("El patrocinador " + patrocinador.nombre + " está interesado en tu club");
                Console.ReadKey();
                string[] options = { "Aceptar patrocinador", "Rechazar patrocinador" };
                var index = SelectWithOptions(options, "Ofrece al club " + patrocinador.semanal + " euros semanales y " + patrocinador.anual + " euros por temporada");
                if (index == 0)
                {
                    DB.query($"UPDATE Club SET patrocinador='{patrocinador.nombre}' WHERE id='{club.id}'");
                    DB.query($"UPDATE Club SET presupuesto='{club.presupuesto + patrocinador.anual}' WHERE id='{club.id}'");
                    Util.Speak(patrocinador.nombre + " es ahora tu patrocinador y te ha ingresado " + patrocinador.anual + " euros");
                    Console.ReadKey();
                }
                else
                {
                    Util.Speak("Patrocinador rechazado");
                    Console.ReadKey();
                }
            }
            //sumaBeneficiosPatrocinador
            if (!club.patrocinador.Equals("Sin patrocinador"))
            {
                var list = Globals.lPatrocinadores.Select(n => n.nombre).ToList();
                var patrocinador = Globals.lPatrocinadores[list.IndexOf(club.patrocinador)];
                DB.query($"UPDATE Club SET presupuesto='{club.presupuesto + patrocinador.semanal}' WHERE id='{club.id}'");
            }
        }
        private static string Entrada(string sp)
        {
            try
            {
                int cantidad = int.Parse(sp.Trim());
                var club = DB.GetClub(DB.GetClub(new Control().clubActual));
                DB.query($"UPDATE Club SET precioEntrada='{cantidad}' WHERE id='{club.id}'");
                return "Precio de entrada cambiado";
            }
            catch (FormatException)
            {
                return "El comando es entrada cantidad";
            }
        }
        private static string OfertaPorTraspaso()
        {
            List<string> clubs = Globals.nombresClubs.ToList();
            clubs.Remove(new Control().clubActual);
            bool isOferta = false;
            var r = new Random();
            int cantidad = 0;
            Jugador jug = new Jugador();
            int idJugador = 0;
            var q = DB.query($"SELECT * FROM Jugador WHERE club='{new Control().clubActual}' AND enTraspaso='1'");
            int variable = 10;
            if (Lector.isPretemporada) variable = 3;
            foreach (var o in q)
            {
              //  idOferta = int.Parse(o[0]);
                idJugador = int.Parse(o[6]);
                jug = DB.GetJugadorId(idJugador);
                cantidad = r.Next(jug.clausula / 3, (int)(jug.clausula * 1.1f));
                if(jug.media>80 && jug.edad<22)
                {
                    isOferta = true;
                    break;
                    //Hacer oferta
                } else
                {
                    if(r.Next(0,variable)==2)
                    {
                        isOferta = true;
                        break;
                        //Hacer oferta
                    }
                }

            }
            if (!isOferta) return "";
            var clubOferta = clubs[r.Next(0, clubs.Count)];
            string pre ="¡Atención! El club "+clubOferta+" ha ofrecido una oferta de " + cantidad + " euros por el jugador " + jug.nombre + " con dorsal " + jug.dorsal+", edad "+jug.edad+" años, media "+jug.media;
            bool fin = false;
            string[] opciones = { "Aceptar oferta por "+ jug.nombre+" de " +cantidad+" euros cuando su cláusula es de "+jug.clausula+" euros", "Rechazar oferta" };
            while(!fin)
            {
                int index = SelectWithOptions(opciones, pre);
                if(index==0)
                {
                    var dorsal = jug.dorsal;
                    while (new Control().dorsales.Contains(dorsal))
                    {
                        dorsal = r.Next(1, 100);
                    }
                    DB.query($"UPDATE Jugador SET club='{clubOferta}', dorsal='{dorsal}', enTraspaso='0'" +
                           $", convocado='0', titular='0', contratoRestante='4' WHERE id='{idJugador}'");
                    q = DB.query($"SELECT presupuesto FROM Club WHERE nombre='{clubOferta}'");
                    int presupuesto = int.Parse(q[0][0]);
                    DB.query($"UPDATE Club SET presupuesto='{presupuesto - cantidad}' WHERE nombre='{clubOferta}'");
                    q = DB.query($"SELECT presupuesto FROM Club WHERE nombre='{new Control().clubActual}'");
                    presupuesto = int.Parse(q[0][0]);
                    DB.query($"UPDATE Club SET presupuesto='{presupuesto + cantidad}' WHERE nombre='{new Control().clubActual}'");
                    return "Has vendido al jugador " + jug.nombre + " al club " + clubOferta + " por " + cantidad + " euros";
                }
                else
                {
                    
                    return "Oferta rechazada";
                }
            }
            return "";
        }
        private static bool CompruebaConvocados()
        {
            var lJug = DB.GetJugadoresClub(new Control().clubActual);
            var titulares = lJug.Select(n => n).Where(n => n.titular == 1).ToList();
            var convocados = lJug.Select(n => n).Where(n => n.convocado == 1).ToList();
            if (titulares.Count < 11 || convocados.Count < 17)
                return false;
            else 
                return true;
        }
        private static bool CompruebaTotalJugadores()
        {
            var lJug = DB.GetJugadoresClub(new Control().clubActual);
            if (lJug.Count<17)
                return false;
            else 
                return true;
        }
        private static bool CompruebaObjetivo()
        {
            var club = DB.GetClub(DB.GetClub(new Control().clubActual));
            if (club.objetivo.Equals("Primeros puestos"))
            {
                if (int.Parse(Posicion(club)) <= (new Temporada().lClubs.Count / 2) - 1)
                {
                    return true;
                }
            }
            else if (club.objetivo.Equals("Media tabla"))
            {
                if (int.Parse(Posicion(club)) <= (new Temporada().lClubs.Count / 2) + 2)
                {
                    return true;
                }
            }
            return false;
        }
        private static bool CompruebaRuina()
        {
            var club = DB.GetClub(DB.GetClub(new Control().clubActual));            
            if (club.presupuesto < 0)
                return true;
            else
                return false;
        }
        private static void RenovandoContratos()
        {
            
            var club = DB.GetClub(DB.GetClub(new Control().clubActual));
            var listSinContrato = club.lJugadores.Select(n => n).Where(n => n.contratoRestante <= 0).ToList();
            if (listSinContrato.Count <= 0) return;
            Util.Speak("Hay jugadores que finalizan su contrato esta temporada");
            foreach (var j in listSinContrato)
            {
                int sueldo = (int)(j.sueldo * 1.1f);
                string[] opciones = { "Renovar contrato", "Despedir" };
                string pre = "El jugador "+j.nombre+" "+j.puesto+" "+" con dorsal "+j.dorsal+" desea renovar su contrato por 4 años más con sueldo de "+sueldo;
                var index = SelectWithOptions(opciones, pre);
                if(index==1)
                {
                    Util.Speak("Has despedido a " + j.nombre);
                    DB.query($"UPDATE Jugador SET enTraspaso='1', club='Sin club', titular='0', convocado='0' WHERE id='{j.id}'");
                  //  DB.query($"DELETE FROM Oferta WHERE idJugador='{j.id}'");
                } else
                {
                    Util.Speak("Has renovado a " + j.nombre);
                    DB.query($"UPDATE Jugador SET enTraspaso='0', contratoRestante='4', sueldo='{sueldo}' WHERE id='{j.id}'");
                }
                Console.ReadKey();
            }

        }
        private static void JugadoresRetirados()
        {
            var q = DB.query($"SELECT id FROM Jugador WHERE edad>50");
            if (q.Count == 0) return;
            var str = "";
            List<string> lString = new List<string>();
            foreach (var j in q)
            {
                var jug = DB.GetJugadorId(int.Parse(j[0]));
                str = jug.nombre+ " dorsal "+jug.dorsal+ " del club "+jug.club+" se ha retirado con "+jug.edad+" años\n";
                DB.query($"DELETE FROM Jugador WHERE id='{jug.id}'");
                lString.Add(str);
            }
            string[] strs = lString.ToArray();
            Select(strs, "", "Hay jugadores que se retiran esta temporada");
            DB.AddJugadoresRandom(22, "Sin club", 30);
            var list = Globals.nombresClubs.ToList();
            list.Remove(new Control().clubActual);
            q = DB.query($"SELECT id FROM Jugador WHERE club='Sin club'");
            var r = new Random();
            foreach (var j in q)
            {
                if(r.Next(0,5)==2)
                {
                    DB.query($"UPDATE jugador SET club='{list[r.Next(0,list.Count)]}' WHERE id='{j[0]}'");
                }
            }
        }
        private static string[] JugadoresEdad(string sp)
        {
            var split = sp.Split(' ');
            int edad = int.Parse(split[1]);
            var q = DB.query($"SELECT id FROM Jugador WHERE edad>={edad}");
            var str = "";
            List<Jugador> lJugadores = new List<Jugador>();
            List<string> lString = new List<string>();
            foreach (var j in q)
            {
                var jug = DB.GetJugadorId(int.Parse(j[0]));
                str = jug.nombre+ " dorsal "+jug.dorsal+ " del club "+jug.club+", "+jug.edad+ " años\n";
                lJugadores.Add(jug);
                lString.Add(str);
            }
            int[] edades = lJugadores.Select(n => n.edad).ToArray();
            string[] strs = lString.ToArray();
            Array.Sort(edades, strs);
            Array.Reverse(strs);
            return strs;
        }
        private static string Pretemporada()
        {
            if (!CompruebaTotalJugadores())
            {
                Util.Speak("¡Atención! Tienes menos de 17 jugadores, serás explusado si no lo solucionas antes del final de la pretemporada");
                Console.ReadKey();
            }
            if (CompruebaRuina())
            {
                Util.Speak("¡Atención! Tu club está en números rojos. Soluciónalo antes del final de temporada o serás expulsado del club");
                Console.ReadKey();
            }
            var ofertas = CompruebaOfertas();
            if (!String.IsNullOrWhiteSpace(ofertas))
            {
                Util.Speak(ofertas);
                Console.ReadKey();
            }
            RestaSueldosCompruebaPatrocinadores();
            var of = OfertaPorTraspaso();
            if (!String.IsNullOrEmpty(of))
            {
                Util.Speak(of);
                Console.ReadKey();
            }
            DB.AddRandomJugadoresEnTraspaso();
            DB.RemoveRandomJugadoresEnTraspaso();
            int jornada = new Control().jornadaActual+1;
            DB.query($"UPDATE Control SET jornadaActual='{jornada}'");
            if(jornada == 1)
            {
                var club = DB.GetClub(DB.GetClub(new Control().clubActual));
                if (!club.patrocinador.Equals("Sin patrocinador"))
                {
                    var list = Globals.lPatrocinadores.Select(n => n.nombre).ToList();
                    var patrocinador = Globals.lPatrocinadores[list.IndexOf(club.patrocinador)];
                    DB.query($"UPDATE Club SET presupuesto='{club.presupuesto + patrocinador.anual}' WHERE id='{club.id}'");
                    Util.Speak("Has cobrado " + patrocinador.anual + " euros de tu patrocinador");
                    Console.ReadKey();
                }
                isPretemporada = false;
                if (!CompruebaTotalJugadores())
                {
                    Util.Speak("No has podido completar la plantilla y los socios han decidido expulsarte, la partida será borrada para no dejar rastro de lo mal manager que eres, " +
                       "has vuelto al menú de inicio");
                    DB.Reset("");
                    Util.EstadoDeJuego = Util.state.Inicio;
                    DB.query($"UPDATE Control SET primeraPartida='0';");
                    return null;
                }
                else
                {
                    DB.CambiaJugadoresEnTraspaso();
                    return "Empieza la temporada " + new Temporada().nombre;
                }
            }
            return "Jornada "+(jornada+6)+ " de pretemporada, quedan " + (Math.Abs(new Control().jornadaActual) + 1) + " jornadas para el inicio de temporada";

        }
        private static string Jugar(Temporada t,bool solo=false)
        {
            if (!CompruebaConvocados()) return "Debes tener un mínimo de 11 titulares y 6 suplentes para jugar un partido";
            Util.aforosUltimaJornada.Clear();
            Util.clubsLocalesUltimaJornada.Clear();
            Util.PlayAmbient(1);
            var str = "";
            var jornada = t.GetJornada;
            var s = jornada.Split('\n');
            for (int i=0;i<s.Length;i++)
            {
                if (String.IsNullOrWhiteSpace(s[i])) continue;
                var p = new Partido(s[i], solo);
                // if(!solo)Util.Speak(p.resultado);
                if (!solo)
                {
                    var list = p.resultado.Trim().Split('\n').ToList();
                    Util.Speak("Partido " + (i + 1)+" de la jornada "+new Control().jornadaActual+", "+list[0]);
                    Util.SelectFromList(list);
                }
                else
                {
                    if(p.local.nombre.Equals(new Control().clubActual) || p.visitante.nombre.Equals(new Control().clubActual))
                    {
                        var list = p.resultado.Trim().Split('\n').ToList();
                        Util.Speak("Partido " + (i + 1) + " de la jornada " + new Control().jornadaActual + ", " + list[0]);
                        Util.SelectFromList(list);
                    }
                }
                if (i==s.Length-2)
                {
                   // str += p.resultado;
                    Console.WriteLine();
                    str += "\nJornada finalizada, ";
                    str += CompruebaOfertas();
                    RestaSueldosCompruebaPatrocinadores();
                    var of = OfertaPorTraspaso();
                    if(!String.IsNullOrEmpty(of)) str = of;
                    if (CompruebaRuina()) str += ", ¡Atención! Tu club está en números rojos. Soluciónalo antes del final de temporada o serás expulsado del club";
                    DB.AddRandomJugadoresEnTraspaso();
                    DB.RemoveRandomJugadoresEnTraspaso();
                    //break;
                }
                if (!solo)
                {
                    Console.WriteLine();
                    //Console.ReadKey();
                } 
            }
            int siguienteJornada = new Control().jornadaActual + 1;
            if (siguienteJornada > (new Temporada().lClubs.Count - 1) * 2)
            {
                string clasificacionFinal = Clasificacion();
                bool objetivosOK = CompruebaObjetivo();
                string goleadoresFinal = Pichichi();
                int nuevaTemporada = (int.Parse(new Temporada().nombre) + 1);
                Console.WriteLine();
               // str += Clasificacion();
                Console.WriteLine();
                var st = "Nueva temporada " + nuevaTemporada;
                DB.NuevaTemporada(nuevaTemporada.ToString(), nuevaTemporada - 2019, new Control().clubActual);
                //aumento edad de jugadores
                var q = DB.query($"SELECT * FROM Jugador;");
                foreach (var v in q)
                {
                    int edad = int.Parse(v[2]);
                    int contratoRestante = int.Parse(v[14]);
                    if (contratoRestante > 0)
                        contratoRestante = contratoRestante - 1;
                    DB.query($"UPDATE Jugador SET edad='{edad + 1}', contratoRestante='{contratoRestante}' WHERE id='{v[6]}'");
                }
                foreach (var v in new Temporada().lClubs)
                {
                    DB.query($"UPDATE Club SET golesAFavor='{0}',golesEnContra='{0}' WHERE id='{v.id}'");
                }
                var list = st.Split(',');
                var lList = list.ToList();
                lList.Add("Clasificación final\n");
                lList.AddRange(clasificacionFinal.Split('\n'));
                lList.Add("Lista de goleadores\n");
                lList.AddRange(goleadoresFinal.Split('\n'));                
                list = lList.ToArray();
                Select(list, "final de temporada","La temporada ha finalizado, ");
                if(CompruebaRuina())
                {
                    Util.PlayAmbient(0);
                    Util.Speak("Has sido expulsado del " + new Control().clubActual+" por arruinar al club, la partida será borrada para no dejar rastro de lo mal manager que eres, " +
                        "has vuelto al menú de inicio");
                    DB.Reset("");
                    Util.EstadoDeJuego = Util.state.Inicio;
                    DB.query($"UPDATE Control SET primeraPartida='0';");
                    return null;
                }
                if(!objetivosOK)
                {
                    var r = new Random();
                    if(r.Next(0,5)==4)
                    {
                        Util.PlayAmbient(0);
                        Util.Speak("No has cumplido los objetivos y los socios han decidido expulsarte, la partida será borrada para no dejar rastro de lo mal manager que eres, " +
                        "has vuelto al menú de inicio");
                        DB.Reset("");
                        Util.EstadoDeJuego = Util.state.Inicio;
                        DB.query($"UPDATE Control SET primeraPartida='0';");
                        return null;
                    }
                    Util.PlayAmbient(0);
                    Util.Speak("No has cumplido los objetivos pero los socios tienen confianza en tí y te dan otra oportunidad");
                    Console.ReadKey();
                    JugadoresRetirados();
                    Console.ReadKey();
                    RenovandoContratos();
                    Console.ReadKey();
                    CompruebaPatrocinadores();
                    return null;
                } else
                {
                    Util.Speak("El club está contento con tu gestión y estará encantado de que lo gestiones una temporada más");
                    Util.PlayAmbient(0);
                    Console.ReadKey();
                    JugadoresRetirados();
                    Console.ReadKey();
                    RenovandoContratos();
                    Console.ReadKey();
                    CompruebaPatrocinadores();
                    return null;
                }
                //siguienteJornada temporada
            }
            else
            {
                DB.query($"UPDATE Control SET jornadaActual='{siguienteJornada}';");
            }
            Util.PlayAmbient(0);
            return str;
            
        }
    }
}

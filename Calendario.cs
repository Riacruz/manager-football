﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManagerFB
{
    class Calendario
    {
        public List<string> lCalendario;
        public List<int> lPosiciones, lTemporadaId, lIds;
        List<string> lEquipos;
        public Calendario(int temporadaId=-1)
        {
            lCalendario = new List<string>();
            lPosiciones = new List<int>();
            lTemporadaId = new List<int>();
            lIds = new List<int>();
            if (temporadaId > 0)
            {
                var q = DB.query($"SELECT * FROM Calendario WHERE temporadaId='{temporadaId}'");
                foreach (var l in q)
                {
                    lCalendario.Add(l[0]);
                    lPosiciones.Add(int.Parse(l[1]));
                    lIds.Add(int.Parse(l[2]));
                    lTemporadaId.Add(int.Parse(l[3]));
                }
            }
            else
            {
                var q = DB.query("SELECT * FROM Calendario");
                foreach (var l in q)
                {
                    lCalendario.Add(l[0]);
                    lPosiciones.Add(int.Parse(l[1]));
                    lIds.Add(int.Parse(l[2]));
                    lTemporadaId.Add(int.Parse(l[3]));
                }
            }
        }
        public void LeeTotalJornadas()
        {
            Util.Speak("Total de jornadas: "+lCalendario.Count/lEquipos.Count*2);
        }
        public List<string> LeeCalendario(bool showConsole=false)
        {
            if (lCalendario==null) return null;
            //Util.Speak("LeeCalendario");
            List<string> lStr = new List<string>();
            var f = lEquipos.Count / 2;
            var count = 0;
            foreach (var l in lCalendario)
            {
                if(showConsole)if (count % f == 0) Util.Speak("Jornada " + ((count / f)+1));
                count++;
                
                var split = l.Split('-');
                int local = int.Parse(split[0].Trim());
                int visitante = int.Parse(split[1].Trim());
                lStr.Add(lEquipos[local-1]+" - "+lEquipos[visitante-1]);
                if (showConsole) Util.Speak(lEquipos[local - 1] + " - " + lEquipos[visitante - 1]);
            }
            return lStr;
        }
        public List<string> CreaNuevoCalendario(List<string> equipos)
        {
            List<string> arr = new List<string>();
            for (int i=0;i<equipos.Count;i++)
            {
                arr.Add((i + 1).ToString());
            }
            Random rnd = new Random();
            string[] MyRandomArray = equipos.OrderBy(x => rnd.Next()).ToArray();
            lEquipos = MyRandomArray.ToList();
            int[] home = new int[arr.Count];
            int[] visit = new int[arr.Count];
            List<string> jornadasPrimeraVuelta = new List<string>();
            List<string> jornadasSegundaVuelta= new List<string>();

          //  Program prog = new Program(); //ei?
            List<List<string>> jornadas = CreaPreCalendario(arr);
            for (int i = 0; i < jornadas.Count; i++)
            {
                for (int x = 0; x < jornadas[i].Count / 2; x++)
                {
                    var split = jornadas[i][x].Split('-');
                    int local = int.Parse(split[0].Trim());
                    int visitante = int.Parse(split[1].Trim());
                    if (i == 0)
                    {
                       // Util.Speak(jornadas[i][x]);
                        jornadasPrimeraVuelta.Add(jornadas[i][x]);
                        home[local - 1]++;
                        visit[visitante - 1]++;
                    }
                    else
                    {
                        // Util.Speak(home[local - 1] >= home[visitante - 1]);
                        // Util.Speak(home[local - 1] +" "+ home[visitante - 1]);
                        if (home[local - 1] > home[visitante - 1])
                        {
                            string partido = visitante + " - " + local;
                          //  Util.Speak(BuscaSegundaVuelta(partido));
                            jornadasPrimeraVuelta.Add(partido);
                            home[visitante - 1]++;
                            visit[local - 1]++;
                            jornadas[i][x] = partido;
                        }
                        else
                        {
                           // Util.Speak(jornadas[i][x]);
                            jornadasPrimeraVuelta.Add(jornadas[i][x]);
                            home[local - 1]++;
                            visit[visitante - 1]++;
                        }

                    }
                }
            }
         //   Util.Speak("Primera vuelta");
            var f = arr.Count / 2;
            var count = 0;
            foreach (var j in jornadasPrimeraVuelta)
            {
                count++;
               // Util.Speak(j);
               // if (count % f == 0) Util.Speak("---");
            }
            jornadasSegundaVuelta = ConvierteASegundaVuelta(jornadasPrimeraVuelta);

            
            jornadasPrimeraVuelta.AddRange(jornadasSegundaVuelta);
            lCalendario = jornadasPrimeraVuelta;
            return jornadasPrimeraVuelta;
        }
        List<string> ConvierteASegundaVuelta(List<string> arr)
        {
            //   Util.Speak("Segunda vuelta");
            List<string> fin = new List<string>();
            foreach (var a in arr)
            {
                var split = a.Split('-');
                int local = int.Parse(split[0].Trim());
                int visitante = int.Parse(split[1].Trim());
                string partido = visitante + " - " + local;
                //  Util.Speak(partido);
                fin.Add(partido);
            }
            return fin;

        }
        string BuscaSegundaVuelta(string partido, List<List<string>> jornadas)
        {
            for (int i = 0; i < jornadas.Count; i++)
            {
                for (int x = jornadas[i].Count / 2; x < jornadas[i].Count; x++)
                {
                    if (partido.Equals(jornadas[i][x]))
                    {

                        var split = jornadas[i][x].Split('-');
                        int local = int.Parse(split[0].Trim());
                        int visitante = int.Parse(split[1].Trim());
                        jornadas[i][x] = visitante + " - " + local;
                        // Util.Speak("Cambio jornadas["+i+"]["+x+"] a " + visitante + " - " + local);
                    }
                }
            }
            return partido;
        }
        List<List<string>> CreaPreCalendario(List<string> arr)
        {
            List<List<string>> jornadas = new List<List<string>>();
            for (int x = 0; x < arr.Count - 1; x++)
            {
                jornadas.Add(new List<string>());
                // Util.Speak(jornadas.Count + " // " + x);
                //Your code goes here
                for (int i = 0; i < arr.Count; i++)
                {
                    if (i == arr.Count - i - 1)
                    {
                        var partido = arr[i] + " - " + (arr[arr.Count - i]);
                        jornadas[x].Add(partido);
                        //    Util.Speak(partido);
                        partido = arr[i] + " - " + (arr[arr.Count - i - 1]);
                        jornadas[x].Add(partido);
                        //  Util.Speak(arr[i] + " - " + (arr[arr.Count - i - 1]));
                    }
                    else
                    {
                        jornadas[x].Add(arr[i] + " - " + arr[(arr.Count - i - 1)]);
                        //Util.Speak(arr[i] + " - " + arr[(arr.Count - i - 1)]);
                    }
                }
                //Util.Speak("- Siguiente jornada -");
                List<string> temp = new List<string>();
                string str = "";
                for (int i = 0; i < arr.Count; i++)
                {
                    if (i == 1)
                    {
                        str = arr[i];
                    }
                    else
                        temp.Add(arr[i]);
                }
                temp.Add(str);
                arr = temp;
            }
            return jornadas;
        }

    }
}

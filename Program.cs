﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Threading;

namespace ManagerFB
{
    class Program
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);
        public static string line = "";
        public static ConsoleKey key = ConsoleKey.Enter;
        public static bool comandoHablado = false;
        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            switch (sig)
            {
                case CtrlType.CTRL_C_EVENT:
                    Util.Speak("Gracias por jugar. Guardando y cerrando...");
                    return true;
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                    Util.Speak("Gracias por jugar. Guardando y cerrando...");
                    return true;
                default:
                    return false;
            }
        }
        private static void Inicio()
        {
            Console.Title = "";
            Thread thread = new Thread(SpeechToText.Speech2);
            thread.Start();
            //Para el evento de capturar la salida
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
            //Empieza
            var q = DB.query("SELECT * FROM Config");
            if (q.Count < 1)
            {
                DB.query("INSERT INTO Config (musica, velocidadLector) VALUES ('1','1');");
                Util.PlayMusic(1);
            }
            else
            {
                Util.PlayMusic(int.Parse(q[0][1]));
                Util.SpeakSpeed(int.Parse(q[0][2]));
            }
            
            Util.Speak("Bienvenido a " + Globals.version + ". Inserte el comando ayuda para conocer los comandos disponibles");
            if (new Control().jornadaActual > 0) Lector.isPretemporada = false;
            while (line != null && !line.ToLower().Equals("salir"))
            {
                line = "";
                ConsoleKeyInfo keyInfo;
                do
                {
                    
                    keyInfo = Console.ReadKey();
                    key = keyInfo.Key;
                    string letras = "1234567890qwertyuiopñlkjhgfdsazxcvbnm ";
                    if (letras.Contains(keyInfo.KeyChar))
                    {
                        line += keyInfo.KeyChar;
                        Util.Speak(keyInfo.KeyChar.ToString());
                    }
                    if (keyInfo.Key == ConsoleKey.Backspace)
                    {
                        if (String.IsNullOrWhiteSpace(line)) continue;
                        Util.Speak("borrar " + line[line.Length - 1]);
                        line = line.Substring(0, line.Length - 1);
                    }
                    
                }
                while (key != ConsoleKey.Enter);
                // line = Console.ReadLine();
                leeLinea(line);
            }
            Util.Speak("Saliendo de ManagerFB");
            Thread.Sleep(3000);

            
        }
        public static void leeLinea(string line)
        {
            line = line.Trim();
            Util.Speak(line);
            Thread.Sleep(1000);
            Console.WriteLine();
            var lector = "";

            if (Util.EstadoDeJuego == Util.state.Inicio)
                lector = Lector.LeeInicio(line);
            else
                lector = Lector.Lee(line);
            Util.Speak(lector);
            Console.WriteLine();
        }
        public static void Main(string[] args)
        {
            Inicio();            
        }
        
    }
}

